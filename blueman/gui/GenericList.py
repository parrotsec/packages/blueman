<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals
=======
# coding=utf-8
from typing import Dict, Optional
>>>>>>> upstream/master

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
<<<<<<< HEAD
from gi.repository import GObject
=======
>>>>>>> upstream/master


# noinspection PyAttributeOutsideInit
class GenericList(Gtk.TreeView):
<<<<<<< HEAD
    def __init__(self, data):
        GObject.GObject.__init__(self)
=======
    def __init__(self, data, **kwargs):
        super().__init__(**kwargs)
        self.set_name("GenericList")
>>>>>>> upstream/master
        self.selection = self.get_selection()
        self._load(data)

    def _load(self, data):
<<<<<<< HEAD
        self.ids = {}
        self.columns = {}

        types = []
        for i in range(len(data)):
            types.append(data[i][1])

        types = tuple(types)
=======
        self.ids: Dict[str, int] = {}
        self.columns: Dict[str, Gtk.TreeViewColumn] = {}

        types = [row["type"] for row in data]
>>>>>>> upstream/master

        self.liststore = Gtk.ListStore(*types)
        self.set_model(self.liststore)

<<<<<<< HEAD
        for i in range(len(data)):

            self.ids[data[i][0]] = i

            if len(data[i]) == 5 or len(data[i]) == 6:

                column = Gtk.TreeViewColumn(data[i][4])

                column.pack_start(data[i][2], True)
                column.set_attributes(data[i][2], **data[i][3])

                if len(data[i]) == 6:
                    column.set_properties(**data[i][5])

                self.columns[data[i][0]] = column
                self.append_column(column)

    def selected(self):
        (model, iter) = self.selection.get_selected()

        return iter

    def delete(self, id):
        if type(id) == Gtk.TreeIter:
            iter = id
        else:
            iter = self.get_iter(id)

        if iter is None:
            return False
        if self.liststore.iter_is_valid(iter):
            self.liststore.remove(iter)
=======
        for i, row in enumerate(data):
            self.ids[row["id"]] = i

            if "renderer" not in row:
                continue

            column = Gtk.TreeViewColumn()
            column.pack_start(row["renderer"], True)
            column.set_attributes(row["renderer"], **row["render_attrs"])

            if "view_props" in row:
                column.set_properties(**row["view_props"])

            if "celldata_func" in row:
                func, user_data = row["celldata_func"]
                column.set_cell_data_func(row["renderer"], func, user_data)

            self.columns[row["id"]] = column
            self.append_column(column)

    def selected(self):
        (model, tree_iter) = self.selection.get_selected()

        return tree_iter

    def delete(self, iterid):
        if type(iterid) == Gtk.TreeIter:
            tree_iter = iterid
        else:
            tree_iter = self.get_iter(iterid)

        if tree_iter is None:
            return False
        if self.liststore.iter_is_valid(tree_iter):
            self.liststore.remove(tree_iter)
>>>>>>> upstream/master
            return True
        else:
            return False

    def _add(self, **columns):
<<<<<<< HEAD
        items = {}
=======
        items: Dict[int, Optional[Gtk.TreeViewColumn]] = {}
>>>>>>> upstream/master
        for k, v in self.ids.items():
            items[v] = None

        for k, v in columns.items():
            if k in self.ids:
                items[self.ids[k]] = v
            else:
                raise Exception("Invalid key %s" % k)

        return items.values()

    def append(self, **columns):
        vals = self._add(**columns)
        return self.liststore.append(vals)

    def prepend(self, **columns):
        vals = self._add(**columns)
        return self.liststore.prepend(vals)

    def get_conditional(self, **cols):
        ret = []
        matches = 0
        for i in range(len(self.liststore)):
            row = self.get(i)
            for k, v in cols.items():
                if row[k] == v:
                    matches += 1

            if matches == len(cols):
                ret.append(i)

            matches = 0

        return ret

<<<<<<< HEAD
    def set(self, id, **cols):
        if type(id) == Gtk.TreeIter:
            iter = id
        else:
            iter = self.get_iter(id)

        if iter is not None:
            for k, v in cols.items():
                self.liststore.set(iter, self.ids[k], v)
=======
    def set(self, iterid, **cols):
        if type(iterid) == Gtk.TreeIter:
            tree_iter = iterid
        else:
            tree_iter = self.get_iter(iterid)

        if tree_iter is not None:
            for k, v in cols.items():
                self.liststore.set(tree_iter, self.ids[k], v)
>>>>>>> upstream/master
            return True
        else:
            return False

<<<<<<< HEAD
    def get(self, id, *items):
        ret = {}

        if id is not None:
            if type(id) == Gtk.TreeIter:
                iter = id
            else:
                iter = self.get_iter(id)
            if len(items) == 0:
                for k, v in self.ids.items():
                    ret[k] = self.liststore.get(iter, v)[0]
            else:
                for i in range(len(items)):
                    if items[i] in self.ids:
                        ret[items[i]] = self.liststore.get(iter, self.ids[items[i]])[0]
=======
    def get(self, iterid, *items):
        ret = {}

        if iterid is not None:
            if type(iterid) == Gtk.TreeIter:
                tree_iter = iterid
            else:
                tree_iter = self.get_iter(iterid)
            if len(items) == 0:
                for k, v in self.ids.items():
                    ret[k] = self.liststore.get(tree_iter, v)[0]
            else:
                for i in range(len(items)):
                    if items[i] in self.ids:
                        ret[items[i]] = self.liststore.get(tree_iter, self.ids[items[i]])[0]
>>>>>>> upstream/master
        else:
            return False

        return ret

    def get_iter(self, path):
        if path is None:
            return None

        try:
            return self.liststore.get_iter(path)
<<<<<<< HEAD
        except:
=======
        except ValueError:
>>>>>>> upstream/master
            return None

    def clear(self):
        self.liststore.clear()

    def compare(self, iter_a, iter_b):
        if iter_a is not None and iter_b is not None:
            return self.get_model().get_path(iter_a) == self.get_model().get_path(iter_b)
        else:
            return False
