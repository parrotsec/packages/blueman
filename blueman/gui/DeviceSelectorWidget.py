<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.Functions import dprint
=======
# coding=utf-8
from gettext import gettext as _
import os
import logging

from blueman.gui.DeviceSelectorList import DeviceSelectorList
>>>>>>> upstream/master

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
<<<<<<< HEAD
from gi.repository import GObject
import os
from blueman.bluez.Adapter import Adapter
from blueman.Constants import *
from blueman.gui.DeviceSelectorList import DeviceSelectorList


class DeviceSelectorWidget(Gtk.VBox):
    def __init__(self, adapter=None):

        GObject.GObject.__init__(self)

        self.props.spacing = 1
        self.props.vexpand = True
        self.set_size_request(360, 340)

        sw = Gtk.ScrolledWindow()
        self.List = devlist = DeviceSelectorList(adapter)
        if self.List.Adapter != None:
            self.List.DisplayKnownDevices()

        sw.add(devlist)
        sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        sw.set_shadow_type(Gtk.ShadowType.IN)

        self.Builder = Gtk.Builder()
        self.Builder.add_from_file(UI_PATH + "/device-list-widget.ui")

        sitem = self.Builder.get_object("search")

        self.cb_adapters = self.Builder.get_object("adapters")

        cell = Gtk.CellRendererText()

        self.cb_adapters.pack_start(cell, True)
        self.cb_adapters.connect("changed", self.on_adapter_selected)
        self.cb_adapters.add_attribute(cell, 'text', 0)

        button = self.Builder.get_object("b_search")
        button.connect("clicked", self.on_search_clicked)

        self.pbar = self.Builder.get_object("progressbar1")

        self.List.connect("discovery-progress", self.on_discovery_progress)

        self.pack_start(sw, True, True, 0)
        self.pack_start(sitem, False, False, 0)

        sitem.show()

        sw.show_all()
=======


class DeviceSelectorWidget(Gtk.Box):
    def __init__(self, adapter_name=None, orientation=Gtk.Orientation.VERTICAL, **kwargs):

        super().__init__(orientation=orientation, spacing=1, vexpand=True,
                         width_request=360, height_request=340,
                         name="DeviceSelectorWidget", **kwargs)

        self.List = DeviceSelectorList(adapter_name)
        if self.List.Adapter is not None:
            self.List.display_known_devices()
            self.List.Adapter.start_discovery()

        sw = Gtk.ScrolledWindow(hscrollbar_policy=Gtk.PolicyType.NEVER,
                                vscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
                                shadow_type=Gtk.ShadowType.IN)
        sw.add(self.List)
        self.pack_start(sw, True, True, 0)

        # Disable overlay scrolling
        if Gtk.get_minor_version() >= 16:
            sw.props.overlay_scrolling = False

        model = Gtk.ListStore(str, str)
        cell = Gtk.CellRendererText()
        self.cb_adapters = Gtk.ComboBox(model=model, visible=True)
        self.cb_adapters.set_tooltip_text(_("Adapter selection"))
        self.cb_adapters.pack_start(cell, True)
        self.cb_adapters.add_attribute(cell, 'text', 0)

        spinner_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6, height_request=8)
        self.spinner = Gtk.Spinner(halign=Gtk.Align.START, hexpand=True, has_tooltip=True,
                                   tooltip_text="Discovering…", margin=6)

        spinner_box.add(self.cb_adapters)
        spinner_box.add(self.spinner)
        self.add(spinner_box)

        self.cb_adapters.connect("changed", self.on_adapter_selected)
>>>>>>> upstream/master

        self.List.connect("adapter-changed", self.on_adapter_changed)
        self.List.connect("adapter-added", self.on_adapter_added)
        self.List.connect("adapter-removed", self.on_adapter_removed)
        self.List.connect("adapter-property-changed", self.on_adapter_prop_changed)

        self.update_adapters_list()
<<<<<<< HEAD

    def __del__(self):
        self.List.destroy()
        dprint("Deleting widget")

    def on_discovery_progress(self, devlist, fraction):
        self.pbar.props.fraction = fraction

    def on_search_clicked(self, button):
        self.List.DiscoverDevices()
=======
        self.show_all()

    def __del__(self):
        self.List.destroy()
        logging.debug("Deleting widget")
>>>>>>> upstream/master

    def on_adapter_prop_changed(self, devlist, adapter, key_value):
        key, value = key_value
        if key == "Name" or key == "Alias":
            self.update_adapters_list()
        elif key == "Discovering":
            if not value:
<<<<<<< HEAD
                self.pbar.props.fraction = 0
=======
                self.spinner.stop()
            else:
                self.spinner.start()
>>>>>>> upstream/master

    def on_adapter_added(self, devlist, adapter_path):
        self.update_adapters_list()

    def on_adapter_removed(self, devlist, adapter_path):
        self.update_adapters_list()

    def on_adapter_selected(self, cb_adapters):
<<<<<<< HEAD
        dprint("selected")
        iter = cb_adapters.get_active_iter()
        if iter:
            adapter_path = cb_adapters.get_model().get_value(iter, 1)
            if self.List.Adapter:
                if self.List.Adapter.get_object_path() != adapter_path:
                    self.List.SetAdapter(os.path.basename(adapter_path))

    def on_adapter_changed(self, devlist, adapter_path):
        dprint("changed")
=======
        logging.info("selected")
        tree_iter = cb_adapters.get_active_iter()
        if tree_iter:
            adapter_path = cb_adapters.get_model().get_value(tree_iter, 1)
            if self.List.Adapter:
                if self.List.Adapter.get_object_path() != adapter_path:
                    # Stop discovering on previous adapter
                    self.List.Adapter.stop_discovery()
                    self.List.set_adapter(os.path.basename(adapter_path))
                    # Start discovery on selected adapter
                    self.List.Adapter.start_discovery()

    def on_adapter_changed(self, devlist, adapter_path):
        logging.info("changed")
>>>>>>> upstream/master
        if adapter_path is None:
            self.update_adapters_list()
        else:
            if self.List.Adapter:
<<<<<<< HEAD
                self.List.DisplayKnownDevices()
=======
                self.List.display_known_devices()
>>>>>>> upstream/master

    def update_adapters_list(self):

        self.cb_adapters.get_model().clear()
<<<<<<< HEAD
        adapters = self.List.manager.list_adapters()
=======
        adapters = self.List.manager.get_adapters()
>>>>>>> upstream/master
        num = len(adapters)
        if num == 0:
            self.cb_adapters.props.visible = False
            self.List.props.sensitive = False
        elif num == 1:
            self.cb_adapters.props.visible = False
            self.List.props.sensitive = True
        elif num > 1:
            self.List.props.sensitive = True
            self.cb_adapters.props.visible = True
            for adapter in adapters:
<<<<<<< HEAD
                iter = self.cb_adapters.get_model().append([adapter.get_name(), adapter.get_object_path()])
                if adapter.get_object_path() == self.List.Adapter.get_object_path():
                    self.cb_adapters.set_active_iter(iter)
=======
                tree_iter = self.cb_adapters.get_model().append([adapter.get_name(), adapter.get_object_path()])
                if self.List.Adapter and adapter.get_object_path() == self.List.Adapter.get_object_path():
                    self.cb_adapters.set_active_iter(tree_iter)
>>>>>>> upstream/master
