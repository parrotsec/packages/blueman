<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

import gi
=======
# coding=utf-8
from gettext import gettext as _
import logging
from typing import List

import gi

from blueman.typing import GSignals

>>>>>>> upstream/master
gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
from gi.repository import Pango
from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Gdk
<<<<<<< HEAD
from blueman.Functions import get_icon, dprint
from blueman.main.SignalTracker import SignalTracker


class ManagerProgressbar(GObject.GObject):
    __gsignals__ = {
    str('cancelled'): (GObject.SignalFlags.RUN_LAST, None, ()),
    }
    __instances__ = []

    def __init__(self, blueman, cancellable=True, text=_("Connecting")):
        def on_enter(evbox, event):
            c = Gdk.Cursor.new(Gdk.CursorType.HAND2)
            self.window.get_window().set_cursor(c)

        def on_leave(evbox, event):
            self.window.get_window().set_cursor(None)

        def on_clicked(evbox, event):
            self.eventbox.props.sensitive = False
            self.emit("cancelled")

        GObject.GObject.__init__(self)
=======
from gi.repository import GLib


class ManagerProgressbar(GObject.GObject):
    __gsignals__: GSignals = {
        'cancelled': (GObject.SignalFlags.RUN_LAST, None, ()),
    }
    __instances__: List["ManagerProgressbar"] = []

    def __init__(self, blueman, cancellable=True, text=_("Connecting")):
        super().__init__()
>>>>>>> upstream/master
        self.Blueman = blueman

        self.cancellable = cancellable

<<<<<<< HEAD
        self.hbox = hbox = blueman.Builder.get_object("statusbar1")

        self.progressbar = Gtk.ProgressBar()

        self.signals = SignalTracker()

        self.button = Gtk.Image.new_from_pixbuf(get_icon("process-stop", 16))
=======
        self.hbox = hbox = blueman.Builder.get_object("status_data")

        self.progressbar = Gtk.ProgressBar()
        self.progressbar.set_name("ManagerProgressbar")

        self._signals: List[int] = []

        self.button = Gtk.Image(icon_name="process-stop", pixel_size=16)
>>>>>>> upstream/master

        self.eventbox = eventbox = Gtk.EventBox()
        eventbox.add(self.button)
        eventbox.props.tooltip_text = _("Cancel Operation")
<<<<<<< HEAD
        self.signals.Handle(eventbox, "enter-notify-event", on_enter)
        self.signals.Handle(eventbox, "leave-notify-event", on_leave)
        self.signals.Handle(eventbox, "button-press-event", on_clicked)
=======
        eventbox.connect("enter-notify-event", self._on_enter)
        eventbox.connect("leave-notify-event", self._on_leave)
        eventbox.connect("button-press-event", self._on_clicked)
>>>>>>> upstream/master

        self.progressbar.set_size_request(100, 15)
        self.progressbar.set_ellipsize(Pango.EllipsizeMode.END)
        self.progressbar.set_text(text)
        self.progressbar.set_pulse_step(0.05)

<<<<<<< HEAD
        self.window = blueman.Builder.get_object("window")

        hbox.pack_end(eventbox, True, False, 0)
        hbox.pack_end(self.progressbar, False, False, 0)

        if ManagerProgressbar.__instances__ != []:
            dprint("hiding", ManagerProgressbar.__instances__[-1])
=======
        hbox.pack_end(eventbox, True, False, 0)
        hbox.pack_end(self.progressbar, False, False, 0)

        if ManagerProgressbar.__instances__:
            logging.info("hiding %s" % ManagerProgressbar.__instances__[-1])
>>>>>>> upstream/master
            ManagerProgressbar.__instances__[-1].hide()

        self.show()
        if not self.cancellable:
            self.eventbox.props.sensitive = False

<<<<<<< HEAD
        self.gsource = None
=======
        self.pulsing = False
>>>>>>> upstream/master
        self.finalized = False

        ManagerProgressbar.__instances__.append(self)

<<<<<<< HEAD
    def connect(self, *args):
        self.signals.Handle("gobject", super(ManagerProgressbar, self), *args)
=======
    def _on_enter(self, evbox, event):
        c = Gdk.Cursor.new(Gdk.CursorType.HAND2)
        self.Blueman.get_window().set_cursor(c)

    def _on_leave(self, evbox, event):
        self.Blueman.get_window().set_cursor(None)

    def _on_clicked(self, evbox, event):
        self.eventbox.props.sensitive = False
        self.emit("cancelled")

    def connect(self, *args):
        self._signals.append(super(ManagerProgressbar, self).connect(*args))
>>>>>>> upstream/master

    def show(self):
        if not self.Blueman.Config["show-statusbar"]:
            self.Blueman.Builder.get_object("statusbar").props.visible = True

<<<<<<< HEAD

        # if self.Blueman.Stats.hbox.size_request()[0] + self.progressbar.size_request()[0] + 16 > self.Blueman.window.get_size()[0]:
        #	self.Blueman.Stats.hbox.hide_all()


=======
>>>>>>> upstream/master
        self.progressbar.props.visible = True
        self.eventbox.props.visible = True
        self.button.props.visible = True

    def hide(self):
        self.Blueman.Stats.hbox.show_all()
        self.progressbar.props.visible = False
        self.eventbox.props.visible = False
        self.button.props.visible = False

    def message(self, msg, timeout=1500):
        self.stop()
        self.set_label(msg)
        self.set_cancellable(False)
<<<<<<< HEAD
        GObject.timeout_add(timeout, self.finalize)

=======
        GLib.timeout_add(timeout, self.finalize)
>>>>>>> upstream/master

    def finalize(self):
        if not self.finalized:
            self.hide()
            self.stop()
<<<<<<< HEAD
            Gdk.Window.set_cursor(self.window.get_window(), None)
=======
            self.Blueman.get_window().set_cursor(None)
>>>>>>> upstream/master
            self.hbox.remove(self.eventbox)
            self.hbox.remove(self.progressbar)
            # self.hbox.remove(self.seperator)
            self.finalized = True

            if ManagerProgressbar.__instances__[-1] == self:
                ManagerProgressbar.__instances__.pop()
<<<<<<< HEAD
                #remove all finalized instances
=======
                # remove all finalized instances
>>>>>>> upstream/master
                for inst in reversed(ManagerProgressbar.__instances__):
                    if inst.finalized:
                        ManagerProgressbar.__instances__.pop()
                    else:
<<<<<<< HEAD
                        #show last active progress bar
                        inst.show()
                        break

            if ManagerProgressbar.__instances__ == []:
                if not self.Blueman.Config["show-statusbar"]:
                    self.Blueman.Builder.get_object("statusbar").props.visible = False

            self.signals.DisconnectAll()

=======
                        # show last active progress bar
                        inst.show()
                        break

            if not ManagerProgressbar.__instances__:
                if not self.Blueman.Config["show-statusbar"]:
                    self.Blueman.Builder.get_object("statusbar").props.visible = False

            for sig in self._signals:
                self.disconnect(sig)
            self._signals = []
>>>>>>> upstream/master

    def set_cancellable(self, b, hide=False):
        if b:
            self.eventbox.props.visible = True
            self.eventbox.props.sensitive = True
        else:
            if hide:
                self.eventbox.props.visible = False
            else:
                self.eventbox.props.sensitive = False

    def set_label(self, label):
        self.progressbar.props.text = label

    def fraction(self, frac):
        if not self.finalized:
            self.progressbar.set_fraction(frac)

    def started(self):
<<<<<<< HEAD
        return self.gsource != None
=======
        return self.gsource is not None
>>>>>>> upstream/master

    def start(self):
        def pulse():
            self.progressbar.pulse()
<<<<<<< HEAD
            return True

        if not self.gsource:
            self.gsource = GObject.timeout_add(1000 / 24, pulse)

    def stop(self):
        if self.gsource != None:
            GObject.source_remove(self.gsource)
=======
            return self.pulsing

        if not self.pulsing:
            self.pulsing = True
            GLib.timeout_add(1000 / 24, pulse)

    def stop(self):
        self.pulsing = False
>>>>>>> upstream/master
        self.progressbar.set_fraction(0.0)
