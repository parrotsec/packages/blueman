<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals
=======
# coding=utf-8
from gettext import gettext as _
import html
import logging
import cairo
import os
>>>>>>> upstream/master

from blueman.gui.DeviceList import DeviceList
from blueman.DeviceClass import get_minor_class, get_major_class, gatt_appearance_to_name
from blueman.gui.manager.ManagerDeviceMenu import ManagerDeviceMenu
<<<<<<< HEAD
from blueman.Sdp import *
=======
from blueman.Constants import PIXMAP_PATH
from blueman.Functions import launch
from blueman.Sdp import ServiceUUID, OBEX_OBJPUSH_SVCLASS_ID
from blueman.gui.GtkAnimation import TreeRowColorFade, TreeRowFade, CellFade
from blueman.main.Config import Config
from _blueman import ConnInfoReadError
>>>>>>> upstream/master

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
<<<<<<< HEAD
from gi.repository import Pango
from blueman.Constants import *
from blueman.Functions import *
import cgi

from blueman.gui.GtkAnimation import TreeRowColorFade, TreeRowFade, CellFade
=======
from gi.repository import GObject
from gi.repository import Pango
>>>>>>> upstream/master


class ManagerDeviceList(DeviceList):
    def __init__(self, adapter=None, inst=None):
        cr = Gtk.CellRendererText()
        cr.props.ellipsize = Pango.EllipsizeMode.END
<<<<<<< HEAD
        data = [
            # device picture
            ["device_pb", GdkPixbuf.Pixbuf, Gtk.CellRendererPixbuf(), {"pixbuf": 0}, None],
            # device caption
            ["caption", str, cr, {"markup": 1}, None, {"expand": True}],


            ["rssi_pb", GdkPixbuf.Pixbuf, Gtk.CellRendererPixbuf(), {"pixbuf": 2}, None, {"spacing": 0}],
            ["lq_pb", GdkPixbuf.Pixbuf, Gtk.CellRendererPixbuf(), {"pixbuf": 3}, None, {"spacing": 0}],
            ["tpl_pb", GdkPixbuf.Pixbuf, Gtk.CellRendererPixbuf(), {"pixbuf": 4}, None, {"spacing": 0}],
            # trusted/bonded icons
            # ["tb_icons", 'PyObject', CellRendererPixbufTable(), {"pixbuffs":5}, None],

            ["connected", bool], # used for quick access instead of device.GetProperties
            ["bonded", bool], # used for quick access instead of device.GetProperties
            ["trusted", bool], # used for quick access instead of device.GetProperties
            ["fake", bool], # used for quick access instead of device.GetProperties,
            # fake determines whether device is "discovered" or a real bluez device

            ["rssi", float],
            ["lq", float],
            ["tpl", float],
            ["orig_icon", GdkPixbuf.Pixbuf],
            ["cell_fader", GObject.TYPE_PYOBJECT],
            ["row_fader", GObject.TYPE_PYOBJECT],
            ["levels_visible", bool],
            ["initial_anim", bool],
        ]
        DeviceList.__init__(self, adapter, data)
=======
        tabledata = [
            # device picture
            {"id": "device_surface", "type": str, "renderer": Gtk.CellRendererPixbuf(),
             "render_attrs": {}, "celldata_func": (self._set_device_cell_data, None)},
            # device caption
            {"id": "caption", "type": str, "renderer": cr,
             "render_attrs": {"markup": 1}, "view_props": {"expand": True}},
            {"id": "rssi_pb", "type": GdkPixbuf.Pixbuf, "renderer": Gtk.CellRendererPixbuf(),
             "render_attrs": {"pixbuf": 2}, "view_props": {"spacing": 0}},
            {"id": "lq_pb", "type": GdkPixbuf.Pixbuf, "renderer": Gtk.CellRendererPixbuf(),
             "render_attrs": {"pixbuf": 3}, "view_props": {"spacing": 0}},
            {"id": "tpl_pb", "type": GdkPixbuf.Pixbuf, "renderer": Gtk.CellRendererPixbuf(),
             "render_attrs": {"pixbuf": 4}, "view_props": {"spacing": 0}},
            {"id": "alias", "type": str},  # used for quick access instead of device.GetProperties
            {"id": "connected", "type": bool},  # used for quick access instead of device.GetProperties
            {"id": "paired", "type": bool},  # used for quick access instead of device.GetProperties
            {"id": "trusted", "type": bool},  # used for quick access instead of device.GetProperties
            {"id": "objpush", "type": bool},  # used to set Send File button
            {"id": "rssi", "type": float},
            {"id": "lq", "type": float},
            {"id": "tpl", "type": float},
            {"id": "icon_info", "type": Gtk.IconInfo},
            {"id": "cell_fader", "type": GObject.TYPE_PYOBJECT},
            {"id": "row_fader", "type": GObject.TYPE_PYOBJECT},
            {"id": "levels_visible", "type": bool},
            {"id": "initial_anim", "type": bool},
        ]
        super().__init__(adapter, tabledata)
        self.set_name("ManagerDeviceList")
>>>>>>> upstream/master
        self.set_headers_visible(False)
        self.props.has_tooltip = True
        self.Blueman = inst

<<<<<<< HEAD
=======
        self.Config = Config("org.blueman.general")
        self.Config.connect('changed', self._on_settings_changed)
        # Set the correct sorting
        self._on_settings_changed(self.Config, "sort-by")
        self._on_settings_changed(self.Config, "sort-type")

>>>>>>> upstream/master
        self.connect("query-tooltip", self.tooltip_query)
        self.tooltip_row = None
        self.tooltip_col = None

        self.connect("button_press_event", self.on_event_clicked)
        self.connect("button_release_event", self.on_event_clicked)

        self.menu = None

        self.connect("drag_data_received", self.drag_recv)
        self.connect("drag-motion", self.drag_motion)

        Gtk.Widget.drag_dest_set(self, Gtk.DestDefaults.ALL, [], Gdk.DragAction.COPY | Gdk.DragAction.DEFAULT)
        Gtk.Widget.drag_dest_add_uri_targets(self)

        self.set_search_equal_func(self.search_func, None)

<<<<<<< HEAD
    def do_device_found(self, device):
        iter = self.find_device(device)
        if iter:
            anim = TreeRowColorFade(self, self.props.model.get_path(iter), Gdk.RGBA(0,0,1,1))
            anim.animate(start=0.8, end=1.0)


    def search_func(self, model, column, key, iter):
        row = self.get(iter, "caption")
        if key.lower() in row["caption"].lower():
            return False
        print(model, column, key, iter)
=======
    def _on_settings_changed(self, settings, key):
        if key in ('sort-by', 'sort-order'):
            sort_by = settings['sort-by']
            sort_order = settings['sort-order']

            if sort_order == 'ascending':
                sort_type = Gtk.SortType.ASCENDING
            else:
                sort_type = Gtk.SortType.DESCENDING

            column_id = self.ids.get(sort_by)

            if column_id:
                self.liststore.set_sort_column_id(column_id, sort_type)

    def on_icon_theme_changed(self, widget):
        for row in self.liststore:
            device = self.get(row.iter, "device")["device"]
            self.row_setup_event(row.iter, device)

    def do_device_found(self, device):
        tree_iter = self.find_device(device)
        if tree_iter:
            anim = TreeRowColorFade(self, self.props.model.get_path(tree_iter), Gdk.RGBA(0, 0, 1, 1))
            anim.animate(start=0.8, end=1.0)

    def search_func(self, model, column, key, tree_iter):
        row = self.get(tree_iter, "caption")
        if key.lower() in row["caption"].lower():
            return False
        logging.info("%s %s %s %s" % (model, column, key, tree_iter))
>>>>>>> upstream/master
        return True

    def drag_recv(self, widget, context, x, y, selection, target_type, time):

        uris = list(selection.get_uris())

        context.finish(True, False, time)

        path = self.get_path_at_pos(x, y)
        if path:
<<<<<<< HEAD
            iter = self.get_iter(path[0])
            device = self.get(iter, "device")["device"]
            command = "blueman-sendto --device=%s" % device.Address
=======
            tree_iter = self.get_iter(path[0])
            device = self.get(tree_iter, "device")["device"]
            command = "blueman-sendto --device=%s" % device['Address']
>>>>>>> upstream/master

            launch(command, uris, False, "blueman", _("File Sender"))
            context.finish(True, False, time)
        else:
            context.finish(False, False, time)

        return True

<<<<<<< HEAD

    def drag_motion(self, widget, drag_context, x, y, timestamp):
        path = self.get_path_at_pos(x, y)
        if path != None:
            if path[0] != self.selected():
                iter = self.get_iter(path[0])
                device = self.get(iter, "device")["device"]
                if not device.Fake:
                    found = False
                    for uuid in device.UUIDs:
                        uuid16 = uuid128_to_uuid16(uuid)
                        if uuid16 == OBEX_OBJPUSH_SVCLASS_ID:
                            found = True
                            break
                    if found:
                        drag_context.drag_status(Gdk.DragAction.COPY, timestamp)
                        self.set_cursor(path[0])
                        return True
                    else:
                        drag_context.drag_status(Gdk.DragAction.DEFAULT, timestamp)
                        return False
                else:
                    drag_context.drag_status(Gdk.DragAction.COPY, timestamp)
                    self.set_cursor(path[0])
                    return True
        else:
            drag_context.drag_status(Gdk.DragAction.DEFAULT, timestamp)
            return False


    def on_event_clicked(self, widget, event):

        if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 3:
            path = self.get_path_at_pos(int(event.x), int(event.y))
            if path != None:
                row = self.get(path[0], "device")

                if row:
                    device = row["device"]
                    if self.Blueman != None:
                        if self.menu == None:
                            self.menu = ManagerDeviceMenu(self.Blueman)

                        self.menu.popup(None, None, None, None, event.button, event.time)


    def get_device_icon(self, klass, iconname):
        if klass != "unknown":
            return get_icon("blueman-" + klass.replace(" ", "-").lower(), 48, "blueman")
        else:
            return get_icon(iconname, 48, "blueman")




    def make_device_icon(self, target, is_bonded=False, is_trusted=False, is_discovered=False, opacity=255):
        if opacity != 255:
            target = opacify_pixbuf(target, opacity)

        sources = []
        if is_bonded:
            sources.append((get_icon("dialog-password", 16), 0, 0, 200))

        if is_trusted:
            sources.append((get_icon("blueman-trust", 16), 0, 32, 200))

        if is_discovered:
            sources.append((get_icon("edit-find", 24), 24, 0, 255))

        return composite_icon(target, sources)


    def device_remove_event(self, device, iter):
        if device.Temp:
            DeviceList.device_remove_event(self, device, iter)
        else:
            row_fader = self.get(iter, "row_fader")["row_fader"]

            def on_finished(fader):

                fader.disconnect(signal)
                fader.freeze()
                DeviceList.device_remove_event(self, device, iter)

            signal = row_fader.connect("animation-finished", on_finished)
            row_fader.thaw()
            self.emit("device-selected", None, None)
            row_fader.animate(start=row_fader.get_state(), end=0.0, duration=400)

    def device_add_event(self, device):
        if device.Fake:
            self.PrependDevice(device)
            GObject.idle_add(self.props.vadjustment.set_value, 0)
            return

        if self.Blueman.Config["latest-last"]:
            self.AppendDevice(device)
        else:
            self.PrependDevice(device)

    def make_caption(self, name, klass, address):
        return "<span size='x-large'>%(0)s</span>\n<span size='small'>%(1)s</span>\n<i>%(2)s</i>" % {"0": cgi.escape(name), "1": klass.capitalize(), "2": address}

    def get_device_class(self, device):
        klass = get_minor_class(device.Class)
        if klass != "uncategorized":
            return get_minor_class(device.Class, True)
        else:
            return get_major_class(device.Class)

    def row_setup_event(self, iter, device):
        if not self.get(iter, "initial_anim")["initial_anim"]:
            cell_fader = CellFade(self, self.props.model.get_path(iter), [2, 3, 4])
            row_fader = TreeRowFade(self, self.props.model.get_path(iter))

            self.set(iter, row_fader=row_fader, cell_fader=cell_fader, levels_visible=False)
=======
    def drag_motion(self, widget, drag_context, x, y, timestamp):
        result = self.get_path_at_pos(x, y)
        if result is not None:
            path = result[0]
            if not self.selection.path_is_selected(path):
                tree_iter = self.get_iter(path)
                has_obj_push = self._has_objpush(self.get(tree_iter, "device")["device"])
                if has_obj_push:
                    Gdk.drag_status(drag_context, Gdk.DragAction.COPY, timestamp)
                    self.set_cursor(path)
                    return True
                else:
                    Gdk.drag_status(drag_context, Gdk.DragAction.DEFAULT, timestamp)
                    return False
        else:
            Gdk.drag_status(drag_context, Gdk.DragAction.DEFAULT, timestamp)
            return False

    def on_event_clicked(self, widget, event):
        if event.type not in (Gdk.EventType._2BUTTON_PRESS, Gdk.EventType.BUTTON_PRESS):
            return

        path = self.get_path_at_pos(int(event.x), int(event.y))
        if path is None:
            return

        row = self.get(path[0], "device", "connected")
        if not row:
            return

        if self.Blueman is None:
            return

        if self.menu is None:
            self.menu = ManagerDeviceMenu(self.Blueman)

        if event.type == Gdk.EventType._2BUTTON_PRESS and event.button == 1:
            if self.menu.show_generic_connect_calc(row["device"]['UUIDs']):
                self.menu.generic_connect(item=None, device=row["device"], connect=not row["connected"])

        if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 3:
            self.menu.popup(None, None, None, None, event.button, event.time)

    def get_icon_info(self, icon_name, size=48, fallback=True):
        if icon_name is None and not fallback:
            return None
        elif icon_name is None and fallback:
            icon_name = "image-missing"

        icon_info = self.icon_theme.lookup_icon_for_scale(icon_name, size, self.get_scale_factor(),
                                                          Gtk.IconLookupFlags.FORCE_SIZE)

        return icon_info

    def make_device_icon(self, icon_info, is_paired=False, is_trusted=False):
        window = self.get_window()
        scale = self.get_scale_factor()
        target = icon_info.load_surface(window)
        ctx = cairo.Context(target)

        if is_paired:
            icon_info = self.get_icon_info("dialog-password", 16, False)
            paired_surface = icon_info.load_surface(window)
            ctx.set_source_surface(paired_surface, 1 / scale, 1 / scale)
            ctx.paint_with_alpha(0.8)

        if is_trusted:
            icon_info = self.get_icon_info("blueman-trust", 16, False)
            trusted_surface = icon_info.load_surface(window)
            height = target.get_height()
            mini_height = trusted_surface.get_height()
            y = height / scale - mini_height / scale - 1 / scale

            ctx.set_source_surface(trusted_surface, 1 / scale, y)
            ctx.paint_with_alpha(0.8)

        return target

    def device_remove_event(self, device):
        tree_iter = self.find_device(device)

        row_fader = self.get(tree_iter, "row_fader")["row_fader"]
        row_fader.connect("animation-finished", self.__on_fader_finished, device, tree_iter)
        row_fader.thaw()
        self.emit("device-selected", None, None)
        row_fader.animate(start=row_fader.get_state(), end=0.0, duration=400)

    def __on_fader_finished(self, fader, device, tree_iter):
        fader.disconnect_by_func(self.__on_fader_finished)
        fader.freeze()
        super().device_remove_event(device)

    def device_add_event(self, device):
        self.add_device(device)

    def make_caption(self, name, klass, address):
        return "<span size='x-large'>%(0)s</span>\n<span size='small'>%(1)s</span>\n<i>%(2)s</i>" \
               % {"0": html.escape(name), "1": klass.capitalize(), "2": address}

    def get_device_class(self, device):
        klass = get_minor_class(device['Class'])
        if klass != "uncategorized":
            return get_minor_class(device['Class'], True)
        else:
            return get_major_class(device['Class'])

    def row_setup_event(self, tree_iter, device):
        if not self.get(tree_iter, "initial_anim")["initial_anim"]:
            cell_fader = CellFade(self, self.props.model.get_path(tree_iter), [2, 3, 4])
            row_fader = TreeRowFade(self, self.props.model.get_path(tree_iter))

            has_objpush = self._has_objpush(device)

            self.set(tree_iter, row_fader=row_fader, cell_fader=cell_fader, levels_visible=False, objpush=has_objpush)
>>>>>>> upstream/master

            cell_fader.freeze()

            def on_finished(fader):
<<<<<<< HEAD
                fader.disconnect(signal)
                fader.freeze()

            signal = row_fader.connect("animation-finished", on_finished)
            row_fader.set_state(0.0)
            row_fader.animate(start=0.0, end=1.0, duration=500)

            self.set(iter, initial_anim=True)

        klass = get_minor_class(device.Class)
        # Bluetooth >= 4 devices use Appearance property
        appearance = device.Appearance
        if klass != "uncategorized" and klass != "unknown":
            icon = self.get_device_icon(klass, device.Icon)
            # get translated version
            description = get_minor_class(device.Class, True).capitalize()
        elif klass == "unknown" and appearance:
            icon = self.get_device_icon(klass, device.Icon)
            description = gatt_appearance_to_name(appearance)
        else:
            icon = get_icon(device.Icon, 48, "blueman")
            description = klass.capitalize()

        name = device.Alias
        address = device.Address

        caption = self.make_caption(name, description, address)

        # caption = "<span size='x-large'>%(0)s</span>\n<span size='small'>%(1)s</span>\n<i>%(2)s</i>" % {"0":name, "1":klass.capitalize(), "2":address}
        self.set(iter, caption=caption, orig_icon=icon)

        self.row_update_event(iter, "Fake", device.Fake)

        try:
            self.row_update_event(iter, "Trusted", device.Trusted)
        except:
            pass
        try:
            self.row_update_event(iter, "Paired", device.Paired)
        except:
            pass


    def row_update_event(self, iter, key, value):
        dprint("row update event", key, value)

        # this property is only emitted when device is fake
        if key == "RSSI":
            row = self.get(iter, "orig_icon")

            #minimum opacity 90
            #maximum opacity 255
            #rssi at minimum opacity -100
            #rssi at maximum opacity -45
            # y = kx + b
            #solve linear system
            #{ 90 = k * -100 + b
            #{ 255 = k * -45 + b
            # k = 3
            # b = 390
            #and we have a formula for opacity based on rssi :)
            opacity = int(3 * value + 390)
            if opacity > 255:
                opacity = 255
            if opacity < 90:
                opacity = 90
            print("opacity", opacity)
            icon = self.make_device_icon(row["orig_icon"], is_discovered=True, opacity=opacity)
            self.set(iter, device_pb=icon)

        elif key == "Trusted":
            row = self.get(iter, "bonded", "orig_icon")
            if value:
                icon = self.make_device_icon(row["orig_icon"], row["bonded"], True, False)
                self.set(iter, device_pb=icon, trusted=True)
            else:
                icon = self.make_device_icon(row["orig_icon"], row["bonded"], False, False)
                self.set(iter, device_pb=icon, trusted=False)


        elif key == "Paired":
            row = self.get(iter, "trusted", "orig_icon")
            if value:
                icon = self.make_device_icon(row["orig_icon"], True, row["trusted"], False)
                self.set(iter, device_pb=icon, bonded=True)
            else:
                icon = self.make_device_icon(row["orig_icon"], False, row["trusted"], False)
                self.set(iter, device_pb=icon, bonded=False)

        elif key == "Fake":
            row = self.get(iter, "bonded", "trusted", "orig_icon")
            if value:
                icon = self.make_device_icon(row["orig_icon"], False, False, True)
                self.set(iter, device_pb=icon, fake=True)
            else:
                icon = self.make_device_icon(row["orig_icon"], row["bonded"], row["trusted"], False)
                self.set(iter, device_pb=icon, fake=False)

        elif key == "Alias":
            device = self.get(iter, "device")["device"]
            c = self.make_caption(value, self.get_device_class(device), device.Address)
            self.set(iter, caption=c)


    def level_setup_event(self, row_ref, device, cinfo):
        def rnd(value):
            return int(round(value, -1))

        if not row_ref.valid():
            return

        iter = self.get_iter(row_ref.get_path())
        if True:
            if cinfo != None:
                try:
                    rssi = float(cinfo.get_rssi())
                except:
                    rssi = 0
                try:
                    lq = float(cinfo.get_lq())
                except:
=======
                fader.disconnect_by_func(on_finished)
                fader.freeze()

            row_fader.connect("animation-finished", on_finished)
            row_fader.set_state(0.0)
            row_fader.animate(start=0.0, end=1.0, duration=500)

            self.set(tree_iter, initial_anim=True)

        klass = get_minor_class(device['Class'])
        # Bluetooth >= 4 devices use Appearance property
        appearance = device["Appearance"]
        if klass != "uncategorized" and klass != "unknown":
            # get translated version
            description = get_minor_class(device['Class'], True).capitalize()
        elif klass == "unknown" and appearance:
            description = gatt_appearance_to_name(appearance)
        else:
            description = get_major_class(device['Class']).capitalize()

        icon_info = self.get_icon_info(device["Icon"], 48, False)
        caption = self.make_caption(device['Alias'], description, device['Address'])

        self.set(tree_iter, caption=caption, icon_info=icon_info, alias=device['Alias'])

        try:
            self.row_update_event(tree_iter, "Trusted", device['Trusted'])
        except Exception as e:
            logging.exception(e)
        try:
            self.row_update_event(tree_iter, "Paired", device['Paired'])
        except Exception as e:
            logging.exception(e)
        try:
            self.row_update_event(tree_iter, "Connected", device["Connected"])
        except Exception as e:
            logging.exception(e)

    def row_update_event(self, tree_iter, key, value):
        logging.info("%s %s" % (key, value))

        if key == "Trusted":
            if value:
                self.set(tree_iter, trusted=True)
            else:
                self.set(tree_iter, trusted=False)

        elif key == "Paired":
            if value:
                self.set(tree_iter, paired=True)
            else:
                self.set(tree_iter, paired=False)

        elif key == "Alias":
            device = self.get(tree_iter, "device")["device"]
            c = self.make_caption(value, self.get_device_class(device), device['Address'])
            self.set(tree_iter, caption=c, alias=value)

        elif key == "UUIDs":
            device = self.get(tree_iter, "device")["device"]
            has_objpush = self._has_objpush(device)
            self.set(tree_iter, objpush=has_objpush)

        elif key == "Connected":
            self.set(tree_iter, connected=value)

    def level_setup_event(self, row_ref, device, cinfo):
        if not row_ref.valid():
            return

        tree_iter = self.get_iter(row_ref.get_path())
        row = self.get(tree_iter, "levels_visible", "cell_fader", "rssi", "lq", "tpl")
        if cinfo is not None:
            # cinfo init may fail for bluetooth devices version 4 and up
            # FIXME Workaround is horrible and we should show something better
            if cinfo.failed:
                rssi_perc = tpl_perc = lq_perc = 100.0
            else:
                try:
                    rssi = float(cinfo.get_rssi())
                except ConnInfoReadError:
                    rssi = 0
                try:
                    lq = float(cinfo.get_lq())
                except ConnInfoReadError:
>>>>>>> upstream/master
                    lq = 0

                try:
                    tpl = float(cinfo.get_tpl())
<<<<<<< HEAD
                except:
=======
                except ConnInfoReadError:
>>>>>>> upstream/master
                    tpl = 0

                rssi_perc = 50 + (rssi / 127 / 2 * 100)
                tpl_perc = 50 + (tpl / 127 / 2 * 100)
                lq_perc = lq / 255 * 100

                if lq_perc < 10:
                    lq_perc = 10
                if rssi_perc < 10:
                    rssi_perc = 10
                if tpl_perc < 10:
                    tpl_perc = 10

<<<<<<< HEAD
                row = self.get(iter, "levels_visible", "cell_fader", "rssi", "lq", "tpl")
                if not row["levels_visible"]:
                    dprint("animating up")
                    self.set(iter, levels_visible=True)
                    fader = row["cell_fader"]
                    fader.thaw()
                    fader.set_state(0.0)
                    fader.animate(start=0.0, end=1.0, duration=400)

                    def on_finished(fader):
                        fader.freeze()
                        fader.disconnect(signal)

                    signal = fader.connect("animation-finished", on_finished)

                if rnd(row["rssi"]) != rnd(rssi_perc):
                    icon = GdkPixbuf.Pixbuf.new_from_file(PIXMAP_PATH + "/blueman-rssi-" + str(rnd(rssi_perc)) + ".png")
                    self.set(iter, rssi_pb=icon)

                if rnd(row["lq"]) != rnd(lq_perc):
                    icon = GdkPixbuf.Pixbuf.new_from_file(PIXMAP_PATH + "/blueman-lq-" + str(rnd(lq_perc)) + ".png")
                    self.set(iter, lq_pb=icon)

                if rnd(row["tpl"]) != rnd(tpl_perc):
                    icon = GdkPixbuf.Pixbuf.new_from_file(PIXMAP_PATH + "/blueman-tpl-" + str(rnd(tpl_perc)) + ".png")
                    self.set(iter, tpl_pb=icon)

                self.set(iter,
                         rssi=rssi_perc,
                         lq=lq_perc,
                         tpl=tpl_perc,
                         connected=True)
            else:

                row = self.get(iter, "levels_visible", "cell_fader")
                if row["levels_visible"]:
                    dprint("animating down")
                    self.set(iter, levels_visible=False,
                             rssi=-1,
                             lq=-1,
                             tpl=-1)
                    fader = row["cell_fader"]
                    fader.thaw()
                    fader.set_state(1.0)
                    fader.animate(start=fader.get_state(), end=0.0, duration=400)

                    def on_finished(fader):
                        fader.disconnect(signal)
                        fader.freeze()
                        if row_ref.valid():
                            self.set(iter, rssi_pb=None, lq_pb=None, tpl_pb=None, connected=False)

                    signal = fader.connect("animation-finished", on_finished)


        else:
            dprint("invisible")


    def tooltip_query(self, tw, x, y, kb, tooltip):

        # print args
        #args[4].set_text("test"+str(args[1]))

=======
            if not row["levels_visible"]:
                logging.info("animating up")
                self.set(tree_iter, levels_visible=True)
                fader = row["cell_fader"]
                fader.thaw()
                fader.set_state(0.0)
                fader.animate(start=0.0, end=1.0, duration=400)

                def on_finished(fader):
                    fader.freeze()
                    fader.disconnect_by_func(on_finished)

                fader.connect("animation-finished", on_finished)

            to_store = {}
            if round(row["rssi"], -1) != round(rssi_perc, -1):
                icon_name = "blueman-rssi-%d.png" % round(rssi_perc, -1)
                icon = GdkPixbuf.Pixbuf.new_from_file(os.path.join(PIXMAP_PATH, icon_name))
                to_store.update({"rssi": rssi_perc, "rssi_pb": icon})

            if round(row["lq"], -1) != round(lq_perc, -1):
                icon_name = "blueman-lq-%d.png" % round(lq_perc, -1)
                icon = GdkPixbuf.Pixbuf.new_from_file(os.path.join(PIXMAP_PATH, icon_name))
                to_store.update({"lq": lq_perc, "lq_pb": icon})

            if round(row["tpl"], -1) != round(tpl_perc, -1):
                icon_name = "blueman-tpl-%d.png" % round(tpl_perc, -1)
                icon = GdkPixbuf.Pixbuf.new_from_file(os.path.join(PIXMAP_PATH, icon_name))
                to_store.update({"tpl": tpl_perc, "tpl_pb": icon})

            if to_store:
                self.set(tree_iter, **to_store)

        else:

            if row["levels_visible"]:
                logging.info("animating down")
                self.set(tree_iter, levels_visible=False,
                         rssi=-1,
                         lq=-1,
                         tpl=-1)
                fader = row["cell_fader"]
                fader.thaw()
                fader.set_state(1.0)
                fader.animate(start=fader.get_state(), end=0.0, duration=400)

                def on_finished(fader):
                    fader.disconnect_by_func(on_finished)
                    fader.freeze()
                    if row_ref.valid():
                        self.set(tree_iter, rssi_pb=None, lq_pb=None, tpl_pb=None)

                fader.connect("animation-finished", on_finished)

    def tooltip_query(self, tw, x, y, kb, tooltip):
>>>>>>> upstream/master
        path = self.get_path_at_pos(x, y)

        if path is not None:
            if path[0] != self.tooltip_row or path[1] != self.tooltip_col:
                self.tooltip_row = path[0]
                self.tooltip_col = path[1]
                return False

<<<<<<< HEAD
            if path[1] == self.columns["device_pb"]:
                iter = self.get_iter(path[0])

                row = self.get(iter, "trusted", "bonded")
                trusted = row["trusted"]
                bonded = row["bonded"]
                if trusted and bonded:
                    tooltip.set_markup(_("<b>Trusted and Bonded</b>"))
                elif bonded:
                    tooltip.set_markup(_("<b>Bonded</b>"))
=======
            if path[1] == self.columns["device_surface"]:
                tree_iter = self.get_iter(path[0])

                row = self.get(tree_iter, "trusted", "paired")
                trusted = row["trusted"]
                paired = row["paired"]
                if trusted and paired:
                    tooltip.set_markup(_("<b>Trusted and Paired</b>"))
                elif paired:
                    tooltip.set_markup(_("<b>Paired</b>"))
>>>>>>> upstream/master
                elif trusted:
                    tooltip.set_markup(_("<b>Trusted</b>"))
                else:
                    return False

                self.tooltip_row = path[0]
                self.tooltip_col = path[1]
                return True

<<<<<<< HEAD
            if path[1] == self.columns["tpl_pb"] or path[1] == self.columns["lq_pb"] or path[1] == self.columns["rssi_pb"]:
                iter = self.get_iter(path[0])

                dt = self.get(iter, "connected")["connected"]
                #print dt
                if dt:
                    rssi = self.get(iter, "rssi")["rssi"]
                    lq = self.get(iter, "lq")["lq"]
                    tpl = self.get(iter, "tpl")["tpl"]

                    if rssi < 30:
                        rssi_state = _("Poor")

                    if rssi < 40 and rssi > 30:
                        rssi_state = _("Sub-optimal")

                    elif rssi > 40 and rssi < 60:
                        rssi_state = _("Optimal")

                    elif rssi > 60:
                        rssi_state = _("Much")

                    elif rssi > 70:
=======
            if path[1] == self.columns["tpl_pb"] \
                    or path[1] == self.columns["lq_pb"] \
                    or path[1] == self.columns["rssi_pb"]:
                tree_iter = self.get_iter(path[0])

                dt = self.get(tree_iter, "connected")["connected"]
                if dt:
                    rssi = self.get(tree_iter, "rssi")["rssi"]
                    lq = self.get(tree_iter, "lq")["lq"]
                    tpl = self.get(tree_iter, "tpl")["tpl"]

                    if rssi < 30:
                        rssi_state = _("Poor")
                    elif rssi < 40:
                        rssi_state = _("Sub-optimal")
                    elif rssi < 60:
                        rssi_state = _("Optimal")
                    elif rssi < 70:
                        rssi_state = _("Much")
                    else:
>>>>>>> upstream/master
                        rssi_state = _("Too much")

                    if tpl < 30:
                        tpl_state = _("Low")
<<<<<<< HEAD

                    if tpl < 40 and tpl > 30:
                        tpl_state = _("Sub-optimal")

                    elif tpl > 40 and rssi < 60:
                        tpl_state = _("Optimal")

                    elif tpl > 60:
                        tpl_state = _("High")

                    elif tpl > 70:
                        tpl_state = _("Very High")

                    if path[1] == self.columns["tpl_pb"]:
                        tooltip.set_markup(_("<b>Connected</b>\nReceived Signal Strength: %(rssi)u%% <i>(%(rssi_state)s)</i>\nLink Quality: %(lq)u%%\n<b>Transmit Power Level: %(tpl)u%%</b> <i>(%(tpl_state)s)</i>") % {"rssi_state": rssi_state, "rssi": rssi, "lq": lq, "tpl": tpl, "tpl_state": tpl_state})
                    elif path[1] == self.columns["lq_pb"]:
                        tooltip.set_markup(_("<b>Connected</b>\nReceived Signal Strength: %(rssi)u%% <i>(%(rssi_state)s)</i>\n<b>Link Quality: %(lq)u%%</b>\nTransmit Power Level: %(tpl)u%% <i>(%(tpl_state)s)</i>") % {"rssi_state": rssi_state, "rssi": rssi, "lq": lq, "tpl": tpl, "tpl_state": tpl_state})
                    elif path[1] == self.columns["rssi_pb"]:
                        tooltip.set_markup(_("<b>Connected</b>\n<b>Received Signal Strength: %(rssi)u%%</b> <i>(%(rssi_state)s)</i>\nLink Quality: %(lq)u%%\nTransmit Power Level: %(tpl)u%% <i>(%(tpl_state)s)</i>") % {"rssi_state": rssi_state, "rssi": rssi, "lq": lq, "tpl": tpl, "tpl_state": tpl_state})

=======
                    elif tpl < 40:
                        tpl_state = _("Sub-optimal")
                    elif tpl < 60:
                        tpl_state = _("Optimal")
                    elif tpl < 70:
                        tpl_state = _("High")
                    else:
                        tpl_state = _("Very High")

                    tooltip_template: str = ""
                    if path[1] == self.columns["tpl_pb"]:
                        tooltip_template = \
                            "<b>Connected</b>\nReceived Signal Strength: %(rssi)u%% <i>(%(rssi_state)s)</i>\n" \
                            "Link Quality: %(lq)u%%\n<b>Transmit Power Level: %(tpl)u%%</b> <i>(%(tpl_state)s)</i>"
                    elif path[1] == self.columns["lq_pb"]:
                        tooltip_template = \
                            "<b>Connected</b>\nReceived Signal Strength: %(rssi)u%% <i>(%(rssi_state)s)</i>\n" \
                            "<b>Link Quality: %(lq)u%%</b>\nTransmit Power Level: %(tpl)u%% <i>(%(tpl_state)s)</i>"
                    elif path[1] == self.columns["rssi_pb"]:
                        tooltip_template = \
                            "<b>Connected</b>\n<b>Received Signal Strength: %(rssi)u%%</b> <i>(%(rssi_state)s)</i>\n" \
                            "Link Quality: %(lq)u%%\nTransmit Power Level: %(tpl)u%% <i>(%(tpl_state)s)</i>"

                    state_dict = {"rssi_state": rssi_state, "rssi": rssi, "lq": lq, "tpl": tpl, "tpl_state": tpl_state}
                    tooltip.set_markup(tooltip_template % state_dict)
>>>>>>> upstream/master
                    self.tooltip_row = path[0]
                    self.tooltip_col = path[1]
                    return True
        return False
<<<<<<< HEAD
=======

    def _has_objpush(self, device):
        if device is None:
            return False

        for uuid in device["UUIDs"]:
            if ServiceUUID(uuid).short_uuid == OBEX_OBJPUSH_SVCLASS_ID:
                return True
        return False

    def _set_device_cell_data(self, col, cell, model, tree_iter, data):
        row = self.get(tree_iter, "icon_info", "trusted", "paired")
        surface = self.make_device_icon(row["icon_info"], row["paired"], row["trusted"])
        cell.set_property("surface", surface)
>>>>>>> upstream/master
