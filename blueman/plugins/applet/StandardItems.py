<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.Functions import launch, create_menuitem, get_icon
=======
# coding=utf-8
from gettext import gettext as _

from blueman.Functions import launch, get_lockfile, get_pid, kill
>>>>>>> upstream/master
from blueman.plugins.AppletPlugin import AppletPlugin
from blueman.gui.CommonUi import show_about_dialog
from blueman.gui.applet.PluginDialog import PluginDialog

import gi
gi.require_version("Gtk", "3.0")
<<<<<<< HEAD
from gi.repository import GObject
=======
>>>>>>> upstream/master
from gi.repository import Gtk


class StandardItems(AppletPlugin):
    __depends__ = ["StatusIcon", "Menu"]
    __unloadable__ = False
    __description__ = _("Adds standard menu items to the status icon menu")
    __author__ = "walmis"

<<<<<<< HEAD
    def on_load(self, applet):
        self.Applet = applet

        applet.Plugins.Menu.Register(self, Gtk.SeparatorMenuItem(), 21)

        self.new_dev = create_menuitem(_("_Setup New Device") + "...", get_icon("document-new", 16))
        self.new_dev.connect("activate", self.on_setup_new)

        self.Applet.Plugins.Menu.Register(self, self.new_dev, 30)

        self.Applet.Plugins.Menu.Register(self, Gtk.SeparatorMenuItem(), 31)

        self.send = create_menuitem(_("Send _Files to Device") + "...", get_icon("blueman-send-file", 16))
        self.send.connect("activate", self.on_send)

        self.Applet.Plugins.Menu.Register(self, self.send, 40)

        self.browse = create_menuitem(_("_Browse Files on Device") + "...", get_icon("document-open", 16))
        self.browse.connect("activate", self.on_browse)

        self.Applet.Plugins.Menu.Register(self, self.browse, 50)

        self.Applet.Plugins.Menu.Register(self, Gtk.SeparatorMenuItem(), 51)

        self.devices = create_menuitem(_("_Devices") + "...", get_icon("blueman", 16))
        self.devices.connect("activate", self.on_devices)

        self.Applet.Plugins.Menu.Register(self, self.devices, 60)

        self.adapters = create_menuitem(_("Adap_ters") + "...", get_icon("blueman-device", 16))
        self.adapters.connect("activate", self.on_adapters)

        self.Applet.Plugins.Menu.Register(self, self.adapters, 70)

        self.services = create_menuitem(_("_Local Services") + "...", get_icon("preferences-desktop", 16))
        self.services.connect("activate", self.on_local_services)

        self.Applet.Plugins.Menu.Register(self, self.services, 80)

        self.Applet.Plugins.Menu.Register(self, Gtk.SeparatorMenuItem(), 81)

        about = create_menuitem("_Help", get_icon('help-about', 16))
        self.Applet.Plugins.Menu.Register(self, about, 90)

        self.plugins = create_menuitem(_("_Plugins"), get_icon("blueman-plugin", 16))
        self.plugins.connect("activate", self.on_plugins)

        self.Applet.Plugins.Menu.Register(self, self.plugins, 85)

        about.connect("activate", self.on_about)

        def on_activate(status_icon):
            self.on_devices(None)


        self.Applet.Plugins.StatusIcon.connect("activate", on_activate)

    def change_sensitivity(self, sensitive):
        try:
            power = self.Applet.Plugins.PowerManager.GetBluetoothStatus()
        except:
            power = True

        sensitive = sensitive and self.Applet.Manager and power
        self.new_dev.props.sensitive = sensitive
        self.send.props.sensitive = sensitive
        self.browse.props.sensitive = sensitive
        self.devices.props.sensitive = sensitive
        self.adapters.props.sensitive = sensitive
=======
    def on_load(self):
        self._plugin_window = None

        self.parent.Plugins.Menu.add(self, 21)

        self.new_dev = self.parent.Plugins.Menu.add(self, 30, text=_("_Set Up New Device") + "…",
                                                    icon_name="document-new", callback=self.on_setup_new)

        self.parent.Plugins.Menu.add(self, 31)

        self.send = self.parent.Plugins.Menu.add(self, 40, text=_("Send _Files to Device") + "…",
                                                 icon_name="blueman-send-file", callback=self.on_send)

        self.parent.Plugins.Menu.add(self, 51)

        self.devices = self.parent.Plugins.Menu.add(self, 60, text=_("_Devices") + "…", icon_name="blueman",
                                                    callback=self.on_devices)

        self.adapters = self.parent.Plugins.Menu.add(self, 70, text=_("Adap_ters") + "…", icon_name="blueman-device",
                                                     callback=self.on_adapters)

        self.parent.Plugins.Menu.add(self, 80, text=_("_Local Services") + "…", icon_name="preferences-desktop",
                                     callback=self.on_local_services)

        self.parent.Plugins.Menu.add(self, 81)

        self.parent.Plugins.Menu.add(self, 90, text="_Help", icon_name='help-about', callback=self.on_about)

        self.parent.Plugins.Menu.add(self, 85, text=_("_Plugins"), icon_name="blueman-plugin", callback=self.on_plugins)

        self.parent.Plugins.StatusIcon.connect("activate", lambda _status_icon: self.on_devices())

    def change_sensitivity(self, sensitive):
        if 'PowerManager' in self.parent.Plugins.get_loaded():
            power = self.parent.Plugins.PowerManager.get_bluetooth_status()
        else:
            power = True

        sensitive = sensitive and self.parent.Manager and power
        self.new_dev.set_sensitive(sensitive)
        self.send.set_sensitive(sensitive)
        self.devices.set_sensitive(sensitive)
        self.adapters.set_sensitive(sensitive)
>>>>>>> upstream/master

    def on_manager_state_changed(self, state):
        self.change_sensitivity(state)

    def on_power_state_changed(self, manager, state):
        self.change_sensitivity(state)

<<<<<<< HEAD

    def on_setup_new(self, menu_item):
        launch("blueman-assistant", None, False, "blueman", _("Bluetooth Assistant"))

    def on_send(self, menu_item):
        launch("blueman-sendto", None, False, "blueman", _("File Sender"))

    def on_browse(self, menu_item):
        launch("blueman-browse", None, False, "blueman", _("File Browser"))

    def on_devices(self, menu_item):
        launch("blueman-manager", None, False, "blueman", _("Device Manager"))

    def on_adapters(self, menu_item):
        launch("blueman-adapters", None, False, "blueman", _("Adapter Preferences"))

    def on_local_services(self, menu_item):
        launch("blueman-services", None, False, "blueman", _("Service Preferences"))

    def on_about(self, menu_item):
        about = show_about_dialog("Blueman " + _("applet"), run=False)

        button = Gtk.Button(_("Plugins"))
        button.set_image(Gtk.Image.new_from_icon_name("blueman-plugin", Gtk.IconSize.BUTTON))
        button.show()

        button.connect("clicked", self.on_plugins)
=======
    def on_setup_new(self):
        launch("blueman-assistant", None, False, "blueman", _("Bluetooth Assistant"))

    def on_send(self):
        launch("blueman-sendto", None, False, "blueman", _("File Sender"))

    def on_devices(self):
        lockfile = get_lockfile('blueman-manager')
        pid = get_pid(lockfile)
        if not pid or not kill(pid, 'blueman-manager'):
            launch("blueman-manager", None, False, "blueman", _("Device Manager"))

    def on_adapters(self):
        launch("blueman-adapters", None, False, "blueman", _("Adapter Preferences"))

    def on_local_services(self):
        launch("blueman-services", None, False, "blueman", _("Service Preferences"))

    def on_about(self):
        about = show_about_dialog("Blueman " + _("applet"), run=False)

        im = Gtk.Image(icon_name="blueman-plugin", pixel_size=16)
        button = Gtk.Button(label=_("Plugins"), visible=True, image=im)

        button.connect("clicked", lambda _button: self.on_plugins())
>>>>>>> upstream/master

        about.action_area.pack_start(button, True, True, 0)
        about.action_area.reorder_child(button, 0)

        about.run()
        about.destroy()

<<<<<<< HEAD
    def on_plugins(self, button):
        dialog = PluginDialog(self.Applet)
        dialog.run()
        dialog.destroy()
=======
    def on_plugins(self):
        def on_close(win, event):
            win.destroy()
            self._plugin_window = None

        if self._plugin_window:
            self._plugin_window.present()
        else:
            self._plugin_window = PluginDialog(self.parent)
            self._plugin_window.connect("delete-event", on_close)
            self._plugin_window.show()
>>>>>>> upstream/master
