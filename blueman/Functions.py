<<<<<<< HEAD
=======
# coding=utf-8
>>>>>>> upstream/master
# Copyright (C) 2008 Valmantas Paliksa <walmis at balticum-tv dot lt>
# Copyright (C) 2008 Tadas Dailyda <tadas at dailyda dot com>
#
# Licensed under the GNU General Public License Version 3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
<<<<<<< HEAD
# 

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals
from io import open

=======
#
from time import sleep
from typing import Optional, Dict, Tuple, List, Callable, Iterable, Union, Any
from types import FrameType
import re
import os
import signal
import atexit
import sys
import errno
from gettext import gettext as _
import logging
import logging.handlers
import argparse
from ctypes import cdll, byref, create_string_buffer
import traceback
import fcntl
import struct
import socket
import array

import cairo

from blueman.main.Config import Config
from blueman.main.DBusProxies import AppletService, DBusProxyFailed
>>>>>>> upstream/master
from blueman.Constants import *

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from gi.repository import GLib
from gi.repository import Gio
<<<<<<< HEAD
import re
import os
import signal
import atexit
import sys
from ctypes import cdll, byref, create_string_buffer
from subprocess import Popen
from gi.repository import GObject
import traceback
try: import __builtin__ as builtins
except ImportError: import builtins

GREEN = lambda x: "\x1b[32;01m" + x + "\x1b[39;49;00m"
BLUE = lambda x: "\x1b[34;01m" + x + "\x1b[39;49;00m"
BOLD = lambda x: "\033[1m" + x + "\033[0m"
YELLOW = lambda x: "\x1b[33;01m" + x + "\x1b[39;49;00m"

import fcntl, struct, termios

try:
    in_fg = os.getpgrp() == struct.unpack(str('h'), fcntl.ioctl(0, termios.TIOCGPGRP, "  "))[0]
except IOError:
    in_fg = 'DEBUG' in os.environ


def dprint(*args):
    #dont print if in the background
    if in_fg:

        s = ""
        for a in args:
            s += ("%s " % a)
        co = sys._getframe(1).f_code

        fname = BOLD(co.co_name)

        print("_________")
        print("%s %s" % (fname, "(%s:%d)" % (co.co_filename, co.co_firstlineno)))
        print(s)
        try:
            sys.stdout.flush()
        except IOError:
            pass


builtins.dprint = dprint

from blueman.main.AppletService import AppletService


def check_bluetooth_status(message, exitfunc, *args, **kwargs):
    try:
        applet = AppletService()
    except:
        print("Blueman applet needs to be running")
        exitfunc()
    if "PowerManager" in applet.QueryPlugins():
        if not applet.GetBluetoothStatus():

            d = Gtk.MessageDialog(None, type=Gtk.MessageType.ERROR)
            d.props.icon_name = "blueman"
            d.props.text = _("Bluetooth Turned Off")
            d.props.secondary_text = message

            d.add_button("Exit", Gtk.ResponseType.NO)
            d.add_button(_("Enable Bluetooth"), Gtk.ResponseType.YES)
            resp = d.run()
            d.destroy()
            if resp != Gtk.ResponseType.YES:
                exitfunc()
            else:
                applet.SetBluetoothStatus(True, *args, **kwargs)


def wait_for_adapter(bluez_adapter, callback, timeout=1000):
    def on_prop_change(key, value):
        if key == "Powered" and value:
            GObject.source_remove(source)
            bluez_adapter.unhandle_signal(on_prop_change, "PropertyChanged")
            callback()

    def on_timeout():
        bluez_adapter.unhandle_signal(on_prop_change, "PropertyChanged")
        GObject.source_remove(source)
        dprint(YELLOW("Warning:"),
               "Bluez didn't provide 'Powered' property in a reasonable timeout\nAssuming adapter is ready")
        callback()

    props = bluez_adapter.get_properties()
    if props["Address"] != "00:00:00:00:00:00":
        callback()
        return

    source = GObject.timeout_add(timeout, on_timeout)
    bluez_adapter.handle_signal(on_prop_change, "PropertyChanged")


def enable_rgba_colormap():
    #screen = Gdk.Display.get_default().get_default_screen()
    #colormap = screen.get_rgba_colormap()
    #if colormap == None:
    #	colormap = screen.get_rgb_colormap()
    #Gtk.widget_set_default_colormap(colormap)
    pass


def launch(cmd, paths=None, system=False, icon_name=None, sn=True, name="blueman"):
    '''Launch a gui app with starup notification'''
=======


__all__ = ["check_bluetooth_status", "launch", "setup_icon_path", "adapter_path_to_name", "e_",
           "format_bytes", "create_menuitem", "get_lockfile", "get_pid", "is_running", "check_single_instance", "kill",
           "have", "set_proc_title", "create_logger", "create_parser", "open_rfcomm", "get_local_interfaces"]


def check_bluetooth_status(message: str, exitfunc: Callable[[], Any]) -> None:
    try:
        applet = AppletService()
    except DBusProxyFailed as e:
        logging.exception(e)
        print("Blueman applet needs to be running")
        exitfunc()
        return

    if "PowerManager" not in applet.QueryPlugins():
        return

    if not applet.GetBluetoothStatus():
        d = Gtk.MessageDialog(
            None, type=Gtk.MessageType.ERROR, icon_name="blueman",
            text=_("Bluetooth Turned Off"), secondary_text=message)
        d.add_button(_("Exit"), Gtk.ResponseType.NO)
        d.add_button(_("Enable Bluetooth"), Gtk.ResponseType.YES)

        resp = d.run()
        d.destroy()

        if resp != Gtk.ResponseType.YES:
            exitfunc()
            return

    applet.SetBluetoothStatus('(b)', True)
    if not applet.GetBluetoothStatus():
        print('Failed to enable bluetooth')
        exitfunc()
        return

    config = Config("org.blueman.plugins.powermanager")
    if config["auto-power-on"] is None:
        d = Gtk.MessageDialog(
            None, type=Gtk.MessageType.QUESTION, icon_name="blueman",
            text=_("Shall bluetooth get enabled automatically?"))
        d.add_button(_("Yes"), Gtk.ResponseType.YES)
        d.add_button(_("No"), Gtk.ResponseType.NO)

        resp = d.run()
        d.destroy()

        config["auto-power-on"] = resp == Gtk.ResponseType.YES


def launch(
    cmd: str,
    paths: Optional[Iterable[str]] = None,
    system: bool = False,
    icon_name: Optional[str] = None,
    name: str = "blueman",
    sn: bool = True,
) -> bool:
    """Launch a gui app with starup notification"""
>>>>>>> upstream/master
    display = Gdk.Display.get_default()
    timestamp = Gtk.get_current_event_time()
    context = display.get_app_launch_context()
    context.set_timestamp(timestamp)
<<<<<<< HEAD
    if sn: flags = Gio.AppInfoCreateFlags.SUPPORTS_STARTUP_NOTIFICATION
    else: flags = Gio.AppInfoCreateFlags.NONE
=======
    if sn:
        flags = Gio.AppInfoCreateFlags.SUPPORTS_STARTUP_NOTIFICATION
    else:
        flags = Gio.AppInfoCreateFlags.NONE
>>>>>>> upstream/master

    env = os.environ
    env["BLUEMAN_EVENT_TIME"] = str(timestamp)

    if not system:
        cmd = os.path.join(BIN_DIR, cmd)
    else:
        cmd = os.path.expanduser(cmd)

    if paths:
<<<<<<< HEAD
        files = [Gio.File.new_for_commandline_arg(p) for p in paths]
=======
        files: Optional[List[Gio.File]] = [Gio.File.new_for_commandline_arg(p) for p in paths]
>>>>>>> upstream/master
    else:
        files = None

    if icon_name:
        icon = Gio.Icon.new_for_string(icon_name)
        context.set_icon(icon)

    appinfo = Gio.AppInfo.create_from_commandline(cmd, name, flags)
<<<<<<< HEAD
    launched = appinfo.launch(files, context)

    if not launched:
        dprint("Command: %s failed" % cmd)

    return launched

def setup_icon_path():
    ic = Gtk.IconTheme.get_default()
    ic.prepend_search_path(ICON_PATH)


def get_icon(name, size=24, fallback="image-missing"):
    ic = Gtk.IconTheme.get_default()

    try:
        icon = ic.load_icon(name, size, 0)
    except GLib.Error:
        if not fallback:
            raise
        try:
            icon = ic.load_icon(fallback, size, 0)
        except GLib.Error:
            icon = ic.load_icon("image-missing", size, 0)

    if icon.props.width > size:
        new_w = size
        new_h = int(size * ( float(icon.props.width) / icon.props.height ))
        icon = icon.scale_simple(new_w, new_h, GdkPixbuf.InterpType.BILINEAR)

    if icon.props.height > size:
        new_w = int(size * ( float(icon.props.height) / icon.props.width ))
        new_h = size
        icon = icon.scale_simple(new_w, new_h, GdkPixbuf.InterpType.BILINEAR)

    return icon


def get_notification_icon(icon, main_icon="blueman"):
    main = get_icon(main_icon, 48)
    sub = get_icon(icon, 24)

    return composite_icon(main, [(sub, 24, 24, 255)])


def adapter_path_to_name(path):
    return re.search(".*(hci[0-9]*)", path).groups(0)[0]

#format error
def e_(msg):
    if isinstance(msg, Exception):
        return str(msg) + "\n" + traceback.format_exc()
    else:
        msg = str(msg)

        s = msg.split(": ")
        del s[0]
        return ": ".join(s)


def opacify_pixbuf(pixbuf, alpha):
    new = pixbuf.copy()
    new.fill(0x00000000)
    pixbuf.composite(new, 0, 0, pixbuf.props.width, pixbuf.props.height, 0, 0, 1, 1, GdkPixbuf.InterpType.BILINEAR, alpha)
    return new

#pixbuf, [(pixbuf, x, y, alpha), (pixbuf, x, y, alpha)]

def composite_icon(target, sources):
    target = target.copy()
    for source in sources:
        source[0].composite(target, source[1], source[2], source[0].get_width(), source[0].get_height(), source[1],
                            source[2], 1, 1, GdkPixbuf.InterpType.NEAREST, source[3])

    return target


def format_bytes(size):
    ret = 0.0
    size = float(size)
    suffix = ""
    if size < 1024:
        ret = size
        suffix = "B"
    elif size > 1024 and size < (1024 * 1024):
        ret = size / 1024
        suffix = "KB"
    elif size > (1024 * 1024) and size < (1024 * 1024 * 1024):
=======
    launched: bool = appinfo.launch(files, context)

    if not launched:
        logging.error("Command: %s failed" % cmd)

    return launched


def setup_icon_path() -> None:
    ic = Gtk.IconTheme.get_default()
    ic.prepend_search_path(ICON_PATH)


def adapter_path_to_name(path: str) -> Optional[str]:
    if path is None or path == '':
        return None

    match = re.search(r".*(hci[0-9]*)", path)
    if match:
        return match.group(1)
    return None


# format error
def e_(msg: Union[str, Exception]) -> Tuple[str, Optional[str]]:
    if isinstance(msg, Exception):
        return str(msg), traceback.format_exc()
    else:
        s = msg.strip().split(": ")[-1]
        return s, None


def format_bytes(size: float) -> Tuple[float, str]:
    size = float(size)
    if size < 1024:
        ret = size
        suffix = "B"
    elif 1024 < size < (1024 * 1024):
        ret = size / 1024
        suffix = "KB"
    elif (1024 * 1024) < size < (1024 * 1024 * 1024):
>>>>>>> upstream/master
        ret = size / (1024 * 1024)
        suffix = "MB"
    else:
        ret = size / (1024 * 1024 * 1024)
        suffix = "GB"

<<<<<<< HEAD
    return (ret, suffix)


def create_menuitem_box(text, pixbuf, orientation=Gtk.Orientation.HORIZONTAL, size=6):
    '''Create a box with icon and label, optinally set size and orientation'''
    item_box = Gtk.Box.new(orientation, size)
    icon = Gtk.Image.new_from_pixbuf(pixbuf)
    label = Gtk.Label.new_with_mnemonic(text)

    item_box.add(icon)
    item_box.add(label)

    return item_box

def create_menuitem(text, pixbuf):
    box = create_menuitem_box(text, pixbuf)
    item = Gtk.MenuItem()
    item.add(box)
=======
    return ret, suffix


def create_menuitem(
    text: str,
    icon_name: Optional[str] = None,
    pixbuf: Optional[GdkPixbuf.Pixbuf] = None,
    surface: Optional[cairo.Surface] = None,
) -> Gtk.ImageMenuItem:
    image = Gtk.Image(pixel_size=16)
    if icon_name:
        image.set_from_icon_name(icon_name, Gtk.IconSize.MENU)
    elif surface:
        image.set_from_surface(surface)
    elif pixbuf:
        image.set_from_pixbuf(pixbuf)
    else:
        raise ValueError("At least provide one of, icon name, surface or pixbuf")

    item = Gtk.ImageMenuItem(label=text, image=image, use_underline=True)
    child = item.get_child()
    child.set_use_markup(True)
>>>>>>> upstream/master
    item.show_all()

    return item


<<<<<<< HEAD
def get_lockfile(name):
    cachedir = GLib.get_user_cache_dir()
    if not os.path.exists(cachedir):
        os.mkdir(cachedir)
    return os.path.join(cachedir, "%s-%s" % (name, os.getuid()))


def get_pid(lockfile):
    f = open(lockfile)
    try:
        return int(f.readline())
    except:
        pass
    finally:
        f.close()


def is_running(name, pid):
    if not os.path.exists("/proc/%s" % pid):
        return False
    f = open("/proc/%s/cmdline" % pid)
    try:
        return name in f.readline().replace("\0", " ")
    finally:
        f.close()


def check_single_instance(name, unhide_func=None):
    print("%s version %s starting" % (name, VERSION))
    lockfile = get_lockfile(name)

    def handler(signum, frame):
        if unhide_func:
            f = open(lockfile)
            f.readline()
            try:
                event_time = int(f.readline())
            except:
                event_time = 0
            f.close()
            unhide_func(event_time)

=======
def get_lockfile(name: str) -> str:
    cachedir = GLib.get_user_cache_dir()
    if not os.path.exists(cachedir):
        try:
            os.mkdir(cachedir)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    return os.path.join(cachedir, "%s-%s" % (name, os.getuid()))


def get_pid(lockfile: str) -> Optional[int]:
    try:
        with open(lockfile, "r") as f:
            return int(f.readline())
    except (ValueError, IOError):
        return None


def is_running(name: str, pid: int) -> bool:
    if not os.path.exists("/proc/%s" % pid):
        return False

    with open("/proc/%s/cmdline" % pid, "r") as f:
        return name in f.readline().replace("\0", " ")


def check_single_instance(name: str, unhide_func: Optional[Callable[[int], Any]] = None) -> None:
    print("%s version %s starting" % (name, VERSION))
    lockfile = get_lockfile(name)

    def handler(_sig: signal.Signals, _frame: FrameType) -> None:
        if unhide_func:
            try:
                with open(lockfile, "r") as f:
                    f.readline()
                    event_time = int(f.readline())
            except ValueError:
                event_time = 0

            unhide_func(event_time)
>>>>>>> upstream/master

    signal.signal(signal.SIGUSR1, handler)

    if os.path.exists(lockfile):
        pid = get_pid(lockfile)
        if pid:
            if not is_running(name, pid):
                print("Stale PID, overwriting")
                os.remove(lockfile)
            else:
                print("There is an instance already running")
                time = os.getenv("BLUEMAN_EVENT_TIME") or 0

<<<<<<< HEAD
                f = open(lockfile, "w")
                f.write("%s\n%s" % (str(pid), str(time)))
                f.close()
=======
                with open(lockfile, "w") as f:
                    f.write("%s\n%s" % (str(pid), str(time)))
>>>>>>> upstream/master

                os.kill(pid, signal.SIGUSR1)
                exit()
        else:
            os.remove(lockfile)

    try:
        fd = os.open(lockfile, os.O_WRONLY | os.O_CREAT | os.O_EXCL, 0o664)
        pid_str = "%s\n%s" % (str(os.getpid()), "0")
        os.write(fd, pid_str.encode("UTF-8"))
        os.close(fd)
    except OSError:
        print("There is an instance already running")
        exit()

    atexit.register(lambda: os.remove(lockfile))


<<<<<<< HEAD
def have(t):
=======
def kill(pid: int, name: str) -> bool:
    if pid and is_running(name, pid):
        print('Terminating ' + name)
        os.kill(pid, signal.SIGTERM)
        return True
    return False


def have(t: str) -> Optional[str]:
>>>>>>> upstream/master
    paths = os.environ['PATH'] + ':/sbin:/usr/sbin'
    for path in paths.split(os.pathsep):
        exec_path = os.path.join(path, t)
        exists = os.path.exists(exec_path)
        executable = os.access(exec_path, os.EX_OK)
        if exists and executable:
            return exec_path
    return None

<<<<<<< HEAD
def mask_ip4_address(ip, subnet):
    masked_ip = bytearray()

    for x, y in zip(bytearray(ip), bytearray(subnet)):
        masked_ip.append(x & y)

    return bytes(masked_ip)

def set_proc_title(name=None):
    '''Set the process title'''
=======

def set_proc_title(name: Optional[str] = None) -> int:
    """Set the process title"""
>>>>>>> upstream/master

    if not name:
        name = os.path.basename(sys.argv[0])

    libc = cdll.LoadLibrary('libc.so.6')
<<<<<<< HEAD
    buff = create_string_buffer(len(name)+1)
    buff.value = name.encode("UTF-8")
    ret = libc.prctl(15, byref(buff), 0, 0, 0)

    if ret != 0:
        dprint("Failed to set process title")

    return ret
=======
    buff = create_string_buffer(len(name) + 1)
    buff.value = name.encode("UTF-8")
    ret: int = libc.prctl(15, byref(buff), 0, 0, 0)

    if ret != 0:
        logging.error("Failed to set process title")

    return ret


logger_format = '%(name)s %(asctime)s %(levelname)-8s %(module)s:%(lineno)s %(funcName)-10s: %(message)s'
syslog_logger_format = '%(name)s %(levelname)s %(module)s:%(lineno)s %(funcName)s: %(message)s'
logger_date_fmt = '%H.%M.%S'


def create_logger(
    log_level: int,
    name: str,
    log_format: Optional[str] = None,
    date_fmt: Optional[str] = None,
    syslog: bool = False,
) -> logging.Logger:
    if log_format is None:
        log_format = logger_format
    if date_fmt is None:
        date_fmt = logger_date_fmt
    logging.basicConfig(level=log_level, format=log_format, datefmt=date_fmt)

    logger = logging.getLogger(None)  # Root logger
    logger.name = name

    if syslog:
        syslog_handler = logging.handlers.SysLogHandler(address="/dev/log")
        syslog_formatter = logging.Formatter(syslog_logger_format)
        syslog_handler.setFormatter(syslog_formatter)
        logger.addHandler(syslog_handler)

    return logger


def create_parser(
    parser: Optional[argparse.ArgumentParser] = None,
    syslog: bool = True,
    loglevel: bool = True,
) -> argparse.ArgumentParser:
    if parser is None:
        parser = argparse.ArgumentParser()

    if loglevel:
        parser.add_argument("--loglevel", dest="LEVEL", default="warning")

    if syslog:
        parser.add_argument("--syslog", dest="syslog", action="store_true")

    return parser


def open_rfcomm(file: str, mode: int) -> int:
    try:
        return os.open(file, mode | os.O_EXCL | os.O_NONBLOCK | os.O_NOCTTY)
    except OSError as err:
        if err.errno == errno.EBUSY:
            logging.warning('%s is busy, delaying 2 seconds' % file)
            sleep(2)
            return open_rfcomm(file, mode)
        else:
            raise


def _netmask_for_ifacename(name: str, sock: socket.socket) -> Optional[str]:
    siocgifnetmask = 0x891b
    bytebuf = struct.pack('256s', name.encode('utf-8'))
    try:
        ret = fcntl.ioctl(sock.fileno(), siocgifnetmask, bytebuf)
    except IOError:
        logging.error('siocgifnetmask failed')
        return None

    return socket.inet_ntoa(ret[20:24])


def get_local_interfaces() -> Dict[str, Tuple[str, Optional[str]]]:
    """ Returns a dictionary of name:ip, mask key value pairs. """
    siocgifconf = 0x8912
    names = array.array('B', 4096 * b'\0')
    names_address, names_length = names.buffer_info()
    mutable_byte_buffer = struct.pack('iL', 4096, names_address)

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            try:
                mutated_byte_buffer = fcntl.ioctl(sock.fileno(), siocgifconf, mutable_byte_buffer)
            except IOError:
                logging.error('siocgifconf failed')
                return {}

            max_bytes_out, names_address_out = struct.unpack('iL', mutated_byte_buffer)
            namestr = names.tobytes()

            ip_dict = {}
            for i in range(0, max_bytes_out, 40):
                name = namestr[i: i + 16].split(b'\0', 1)[0].decode('utf-8')
                ipaddr = socket.inet_ntoa(namestr[i + 20: i + 24])
                mask = _netmask_for_ifacename(name, sock)
                ip_dict[name] = (ipaddr, mask)
    except socket.error:
        logging.error('Socket creation failed', exc_info=True)
        return {}

    return ip_dict
>>>>>>> upstream/master
