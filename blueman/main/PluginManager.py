<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from gi.repository import GObject
import os
try: import __builtin__ as builtins
except ImportError: import builtins
import traceback

from blueman.Functions import *
=======
# coding=utf-8
from gettext import gettext as _
import os
import logging
import traceback
import importlib
from typing import Dict, List, Type, Union

from gi.repository import GObject

from blueman.gui.CommonUi import ErrorDialog
from blueman.main.Config import Config
from blueman.plugins.AppletPlugin import AppletPlugin
from blueman.plugins.ManagerPlugin import ManagerPlugin
from blueman.typing import GSignals
>>>>>>> upstream/master


class StopException(Exception):
    pass


class LoadException(Exception):
    pass


<<<<<<< HEAD
builtins.StopException = StopException


class PluginManager(GObject.GObject):
    __gsignals__ = {
    str('plugin-loaded'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_STRING,)),
    str('plugin-unloaded'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_STRING,)),
    }

    def __init__(self, plugin_class, module_path, user_data):
        GObject.GObject.__init__(self)
        self.__plugins = {}
        self.__classes = {}
        self.__deps = {}
        self.__cfls = {}
        self.__loaded = []
        self.user_data = user_data
=======
class PluginManager(GObject.GObject):
    __gsignals__: GSignals = {
        'plugin-loaded': (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_STRING,)),
        'plugin-unloaded': (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_STRING,)),
    }

    def __init__(self, plugin_class, module_path, parent):
        super().__init__()
        self.__deps: Dict[str, List[str]] = {}
        self.__cfls: Dict[str, List[str]] = {}
        self.__plugins: Dict[str, Union[AppletPlugin, ManagerPlugin]] = {}
        self.__classes: Dict[str, Type[Union[AppletPlugin, ManagerPlugin]]] = {}
        self.__loaded: List[str] = []
        self.parent = parent
>>>>>>> upstream/master

        self.module_path = module_path
        self.plugin_class = plugin_class

    @property
    def config_list(self):
        return []

<<<<<<< HEAD
    def GetClasses(self):
        return self.__classes

    def GetLoaded(self):
        return self.__loaded

    def GetDependencies(self):
        return self.__deps

    def GetConflicts(self):
        return self.__cfls

    def Load(self, name=None, user_action=False):
        if name:
            try:
                self.__load_plugin(self.__classes[name])
            except LoadException as e:
                pass
            except Exception as e:
                if user_action:
                    d = Gtk.MessageDialog(type=Gtk.MessageType.ERROR,
                                          buttons=Gtk.ButtonsType.CLOSE)
                    d.set_markup(_("<b>An error has occured while loading "
                                   "a plugin. Please notify the developers "
                                   "with the content of this message.</b>"))
                    d.props.secondary_text = traceback.format_exc()
=======
    def get_classes(self):
        return self.__classes

    def get_loaded(self):
        return self.__loaded

    def get_dependencies(self):
        return self.__deps

    def get_conflicts(self):
        return self.__cfls

    def load_plugin(self, name=None, user_action=False):
        if name:
            try:
                self.__load_plugin(self.__classes[name])
            except LoadException:
                pass
            except Exception:
                if user_action:
                    d = ErrorDialog(_("<b>An error has occured while loading "
                                      "a plugin. Please notify the developers "
                                      "with the content of this message to our </b>\n"
                                      "<a href=\"http://github.com/blueman-project/blueman/issues\">website.</a>"),
                                    excp=traceback.format_exc())
>>>>>>> upstream/master
                    d.run()
                    d.destroy()
                    raise

            return

        path = os.path.dirname(self.module_path.__file__)
        plugins = []
        for root, dirs, files in os.walk(path):
            for f in files:
                if f.endswith(".py") and not (f.endswith(".pyc") or f.endswith("_.py")):
                    plugins.append(f[0:-3])

<<<<<<< HEAD
        dprint(plugins)
        for plugin in plugins:
            try:
                __import__(self.module_path.__name__ + ".%s" % plugin, None, None, [])
            except ImportError as e:
                dprint("Unable to load plugin module %s\n%s" % (plugin, e))

        for cls in self.plugin_class.__subclasses__():
            self.__classes[cls.__name__] = cls
            if not cls.__name__ in self.__deps:
                self.__deps[cls.__name__] = []

            if not cls.__name__ in self.__cfls:
                self.__cfls[cls.__name__] = []

            for c in cls.__depends__:
                if not c in self.__deps:
=======
        logging.info(plugins)
        for plugin in plugins:
            try:
                importlib.import_module(self.module_path.__name__ + ".%s" % plugin)
            except ImportError:
                logging.error("Unable to load plugin module %s" % plugin, exc_info=True)

        for cls in self.plugin_class.__subclasses__():
            self.__classes[cls.__name__] = cls
            if cls.__name__ not in self.__deps:
                self.__deps[cls.__name__] = []

            if cls.__name__ not in self.__cfls:
                self.__cfls[cls.__name__] = []

            for c in cls.__depends__:
                if c not in self.__deps:
>>>>>>> upstream/master
                    self.__deps[c] = []
                self.__deps[c].append(cls.__name__)

            for c in cls.__conflicts__:
<<<<<<< HEAD
                if not c in self.__cfls:
=======
                if c not in self.__cfls:
>>>>>>> upstream/master
                    self.__cfls[c] = []
                self.__cfls[c].append(cls.__name__)
                if c not in self.__cfls[cls.__name__]:
                    self.__cfls[cls.__name__].append(c)

        c = self.config_list
        for name, cls in self.__classes.items():
            for dep in self.__deps[name]:
<<<<<<< HEAD
                #plugins that are required by not unloadable plugins are not unloadable too
                if not self.__classes[dep].__unloadable__:
                    cls.__unloadable__ = False

            if (cls.__autoload__ or (c and cls.__name__ in c)) and not (cls.__unloadable__ and c and "!" + cls.__name__ in c):
                try:
                    self.__load_plugin(cls)
                except:
                    pass

    def Disabled(self, plugin):
        return False

    def Enabled(self, plugin):
=======
                # plugins that are required by not unloadable plugins are not unloadable too
                if not self.__classes[dep].__unloadable__:
                    cls.__unloadable__ = False

            if (cls.__autoload__ or (c and cls.__name__ in c)) and \
                    not (cls.__unloadable__ and c and "!" + cls.__name__ in c):
                self.__load_plugin(cls)

    def disable_plugin(self, plugin):
        return False

    def enable_plugin(self, plugin):
>>>>>>> upstream/master
        return True

    def __load_plugin(self, cls):
        if cls.__name__ in self.__loaded:
            return

        for dep in cls.__depends__:
<<<<<<< HEAD
            if not dep in self.__loaded:
                if not dep in self.__classes:
=======
            if dep not in self.__loaded:
                if dep not in self.__classes:
>>>>>>> upstream/master
                    raise Exception("Could not satisfy dependency %s -> %s" % (cls.__name__, dep))
                try:
                    self.__load_plugin(self.__classes[dep])
                except Exception as e:
<<<<<<< HEAD
                    dprint(e)
=======
                    logging.exception(e)
>>>>>>> upstream/master
                    raise

        for cfl in self.__cfls[cls.__name__]:
            if cfl in self.__classes:
<<<<<<< HEAD
                if self.__classes[cfl].__priority__ > cls.__priority__ and not self.Disabled(cfl) and not self.Enabled(
                        cls.__name__):
                    dprint("Not loading %s because it's conflict has higher priority" % cls.__name__)
                    return

            if cfl in self.__loaded:
                if cls.__priority__ > self.__classes[cfl].__priority__ and not self.Enabled(cfl):
                    self.Unload(cfl)
                else:
                    raise LoadException("Not loading conflicting plugin %s due to lower priority" % cls.__name__)

        dprint("loading", cls)
        inst = cls(self.user_data)
        try:
            inst._load(self.user_data)
        except Exception as e:
            dprint("Failed to load %s\n%s" % (cls.__name__, e))
            if not cls.__unloadable__:
                os._exit(1)

            raise #NOTE TO SELF: might cause bugs
=======
                if self.__classes[cfl].__priority__ > cls.__priority__ and not self.disable_plugin(cfl) \
                        and not self.enable_plugin(cls.__name__):
                    logging.warning("Not loading %s because its conflict has higher priority" % cls.__name__)
                    return

            if cfl in self.__loaded:
                if cls.__priority__ > self.__classes[cfl].__priority__ and not self.enable_plugin(cfl):
                    self.unload_plugin(cfl)
                else:
                    raise LoadException("Not loading conflicting plugin %s due to lower priority" % cls.__name__)

        logging.info("loading %s" % cls)
        inst = cls(self.parent)
        try:
            inst._load()
        except Exception:
            logging.error("Failed to load %s" % cls.__name__, exc_info=True)
            if not cls.__unloadable__:
                os._exit(1)

            raise  # NOTE TO SELF: might cause bugs
>>>>>>> upstream/master

        else:
            self.__plugins[cls.__name__] = inst

            self.__loaded.append(cls.__name__)
            self.emit("plugin-loaded", cls.__name__)

    def __getattr__(self, key):
        try:
            return self.__plugins[key]
<<<<<<< HEAD
        except:
            return self.__dict__[key]

    def Unload(self, name):
        if self.__classes[name].__unloadable__:
            for d in self.__deps[name]:
                self.Unload(d)

            if name in self.__loaded:
                dprint("Unloading %s" % name)
=======
        except KeyError:
            return self.__dict__[key]

    def unload_plugin(self, name):
        if self.__classes[name].__unloadable__:
            for d in self.__deps[name]:
                self.unload_plugin(d)

            if name in self.__loaded:
                logging.info("Unloading %s" % name)
>>>>>>> upstream/master
                try:
                    inst = self.__plugins[name]
                    inst._unload()
                except NotImplementedError:
<<<<<<< HEAD
                    print("Plugin cannot be unloaded")
=======
                    logging.warning("Plugin cannot be unloaded")
>>>>>>> upstream/master
                else:
                    self.__loaded.remove(name)
                    del self.__plugins[name]
                    self.emit("plugin-unloaded", name)

        else:
            raise Exception("Plugin %s is not unloadable" % name)

<<<<<<< HEAD

    def get_plugins(self):
        return self.__plugins

    #executes a function on all plugin instances
    def Run(self, function, *args, **kwargs):
        rets = []
        for inst in self.__plugins.values():
            try:
                ret = getattr(inst, function)(*args, **kwargs)
                rets.append(ret)
            except Exception as e:
                dprint("Function", function, "on", inst.__class__.__name__, "Failed")
                traceback.print_exc()

        return rets

    #executes a function on all plugin instances, runs a callback after each plugin returns something
    def RunEx(self, function, callback, *args, **kwargs):
        for inst in self.__plugins.values():
            ret = getattr(inst, function)(*args, **kwargs)
=======
    def get_plugins(self):
        return self.__plugins

    # executes a function on all plugin instances
    def run(self, func, *args, **kwargs):
        rets = []
        for inst in self.__plugins.values():
            try:
                ret = getattr(inst, func)(*args, **kwargs)
                rets.append(ret)
            except Exception:
                logging.error("Function %s on %s failed" % (func, inst.__class__.__name__), exc_info=True)

        return rets

    # executes a function on all plugin instances, runs a callback after each plugin returns something
    def run_ex(self, func, callback, *args, **kwargs):
        for inst in self.__plugins.values():
            ret = getattr(inst, func)(*args, **kwargs)
>>>>>>> upstream/master
            try:
                ret = callback(inst, ret)
            except StopException:
                return ret
<<<<<<< HEAD
            except Exception as e:
                dprint("Function", function, "on", inst.__class__.__name__, "Failed")
                traceback.print_exc()
                return

            if ret != None:
                args = ret


try:
    from blueman.main.Config import Config
except:
    pass


class PersistentPluginManager(PluginManager):
    def __init__(self, *args):
        super(PersistentPluginManager, self).__init__(*args)
=======
            except Exception:
                logging.error("Function %s on %s failed" % (func, inst.__class__.__name__), exc_info=True)
                return

            if ret is not None:
                args = ret


class PersistentPluginManager(PluginManager):
    def __init__(self, *args):
        super().__init__(*args)
>>>>>>> upstream/master

        self.__config = Config("org.blueman.general")

        self.__config.connect("changed::plugin-list", self.on_property_changed)

<<<<<<< HEAD
    def Disabled(self, plugin):
        plugins = self.__config["plugin-list"]
        return "!" + plugin in plugins

    def Enabled(self, plugin):
        plugins = self.__config["plugin-list"]
        return plugin in plugins

    def SetConfig(self, plugin, state):
=======
    def disable_plugin(self, plugin):
        plugins = self.__config["plugin-list"]
        return "!" + plugin in plugins

    def enable_plugin(self, plugin):
        plugins = self.__config["plugin-list"]
        return plugin in plugins

    def set_config(self, plugin, state):
>>>>>>> upstream/master
        plugins = self.__config["plugin-list"]
        if plugin in plugins:
            plugins.remove(plugin)
        elif "!" + plugin in plugins:
            plugins.remove("!" + plugin)

        plugins.append(str("!" + plugin) if not state else str(plugin))
        self.__config["plugin-list"] = plugins

    @property
    def config_list(self):
        return self.__config["plugin-list"]

    def on_property_changed(self, config, key):
        for item in config[key]:
            disable = item.startswith("!")
            if disable:
                item = item.lstrip("!")

            try:
<<<<<<< HEAD
                cls = self.GetClasses()[item]
                if not cls.__unloadable__ and disable:
                    print(YELLOW("warning:"), item, "is not unloadable")
                elif item in self.GetLoaded() and disable:
                    self.Unload(item)
                elif item not in self.GetLoaded() and not disable:
                    try:
                        self.Load(item, user_action=True)
                    except:
                        self.SetConfig(item, False)

            except KeyError:
                print(YELLOW("warning:"), "Plugin %s not found" % item)
=======
                cls = self.get_classes()[item]
                if not cls.__unloadable__ and disable:
                    logging.warning("warning: %s is not unloadable" % item)
                elif item in self.get_loaded() and disable:
                    self.unload_plugin(item)
                elif item not in self.get_loaded() and not disable:
                    try:
                        self.load_plugin(item, user_action=True)
                    except Exception as e:
                        logging.exception(e)
                        self.set_config(item, False)

            except KeyError:
                logging.warning("warning: Plugin %s not found" % item)
>>>>>>> upstream/master
                continue
