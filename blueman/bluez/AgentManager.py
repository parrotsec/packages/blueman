<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.bluez.BlueZInterface import BlueZInterface
from blueman.bluez.errors import raise_dbus_error
import dbus


class AgentManager(BlueZInterface):
    @raise_dbus_error
    def __init__(self):
        interface = 'org.bluez.AgentManager1'
        super(AgentManager, self).__init__(interface, '/org/bluez')

    @raise_dbus_error
    def register_agent(self, agent, capability='', default=False):
        path = agent.get_object_path()
        self.get_interface().RegisterAgent(path, capability)
        if default:
            self.get_interface().RequestDefaultAgent(path)

    @raise_dbus_error
    def unregister_agent(self, agent):
        self.get_interface().UnregisterAgent(agent.get_object_path())
=======
# coding=utf-8
from blueman.bluez.Base import Base
from gi.repository import GLib


class AgentManager(Base):
    _interface_name = 'org.bluez.AgentManager1'

    def __init__(self) -> None:
        super().__init__(interface_name=self._interface_name, obj_path='/org/bluez')

    def register_agent(self, agent_path: str, capability: str = "", default: bool = False) -> None:
        param = GLib.Variant('(os)', (agent_path, capability))
        self._call('RegisterAgent', param)
        if default:
            default_param = GLib.Variant('(o)', (agent_path,))
            self._call('RequestDefaultAgent', default_param)

    def unregister_agent(self, agent_path: str) -> None:
        param = GLib.Variant('(o)', (agent_path,))
        self._call('UnregisterAgent', param)
>>>>>>> upstream/master
