<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.Functions import dprint
from blueman.bluez.obex.Base import Base
from gi.repository import GObject


class ObjectPush(Base):
    __gsignals__ = {
        str('transfer-started'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT, GObject.TYPE_PYOBJECT,)),
        str('transfer-failed'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT,)),
    }

    def __init__(self, session_path):
        if self.__class__.get_interface_version()[0] < 5:
            super(ObjectPush, self).__init__('org.bluez.obex.ObjectPush', session_path, True)
        else:
            super(ObjectPush, self).__init__('org.bluez.obex.ObjectPush1', session_path)

    def send_file(self, file_path):
        def on_transfer_started(*params):
            transfer_path, props = params[0] if self.__class__.get_interface_version()[0] < 5 else params
            dprint(self.object_path, file_path, transfer_path)
            self.emit('transfer-started', transfer_path, props['Filename'])

        def on_transfer_error(error):
            dprint(file_path, error)
            self.emit('transfer-failed', error)

        self._interface.SendFile(file_path, reply_handler=on_transfer_started, error_handler=on_transfer_error)

    def get_session_path(self):
        return self.object_path
=======
# coding=utf-8
import logging
from typing import Dict

from blueman.bluez.errors import BluezDBusException
from blueman.bluez.obex.Base import Base
from gi.repository import GObject, GLib

from blueman.typing import GSignals


class ObjectPush(Base):
    __gsignals__: GSignals = {
        'transfer-started': (GObject.SignalFlags.NO_HOOKS, None, (str, str,)),
        'transfer-failed': (GObject.SignalFlags.NO_HOOKS, None, (str,)),
    }

    _interface_name = 'org.bluez.obex.ObjectPush1'

    def __init__(self, session_path: str):
        super().__init__(interface_name=self._interface_name, obj_path=session_path)

    def send_file(self, file_path: str) -> None:
        def on_transfer_started(transfer_path: str, props: Dict[str, str]) -> None:
            logging.info(" ".join((self.get_object_path(), file_path, transfer_path)))
            self.emit('transfer-started', transfer_path, props['Filename'])

        def on_transfer_error(error: BluezDBusException) -> None:
            logging.error("%s %s" % (file_path, error))
            self.emit('transfer-failed', error)

        param = GLib.Variant('(s)', (file_path,))
        self._call('SendFile', param, reply_handler=on_transfer_started, error_handler=on_transfer_error)

    def get_session_path(self) -> str:
        path: str = self.get_object_path()
        return path
>>>>>>> upstream/master
