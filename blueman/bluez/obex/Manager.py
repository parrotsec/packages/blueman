<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.Functions import dprint
from blueman.bluez.obex.Transfer import Transfer
from blueman.bluez.obex.Base import Base
from gi.repository import GObject


class Manager(Base):
    __gsignals__ = {
        str('session-removed'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT,)),
        str('transfer-started'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT,)),
        str('transfer-completed'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT, GObject.TYPE_PYOBJECT)),
    }

    def __init__(self):
        if self.__class__.get_interface_version()[0] < 5:
            super(Manager, self).__init__('org.bluez.obex.Manager', '/')
            handlers = {
                'SessionRemoved': self._on_session_removed,
                'TransferStarted': self._on_transfer_started,
                'TransferCompleted': self._on_transfer_completed
            }

            for signal, handler in handlers.items():
                self._handle_signal(handler, signal, )

        else:
            super(Manager, self).__init__('org.freedesktop.DBus.ObjectManager', '/')

            self._transfers = {}

            def on_interfaces_added(object_path, interfaces):
                if 'org.bluez.obex.Transfer1' in interfaces:
                    def on_tranfer_completed(_transfer):
                        self._on_transfer_completed(object_path, True)

                    def on_tranfer_error(_transfer, _msg):
                        self._on_transfer_completed(object_path, False)

                    self._transfers[object_path] = Transfer(object_path)
                    self._transfers[object_path].connect('completed', on_tranfer_completed)
                    self._transfers[object_path].connect('error', on_tranfer_error)
                    self._on_transfer_started(object_path)

            self._handle_signal(on_interfaces_added, 'InterfacesAdded')

            def on_interfaces_removed(object_path, interfaces):
                if 'org.bluez.obex.Session1' in interfaces:
                    self._on_session_removed(object_path)

            self._handle_signal(on_interfaces_removed, 'InterfacesRemoved')

    def _on_session_removed(self, session_path):
        dprint(session_path)
        self.emit('session-removed', session_path)

    def _on_transfer_started(self, transfer_path):
        dprint(transfer_path)
        self.emit('transfer-started', transfer_path)

    def _on_transfer_completed(self, transfer_path, success):
        dprint(transfer_path, success)
        self.emit('transfer-completed', transfer_path, success)
=======
# coding=utf-8
import logging
import weakref
from typing import Dict, Callable

from gi.repository import GObject, Gio

from blueman.bluez.obex.Transfer import Transfer
from blueman.gobject import SingletonGObjectMeta
from blueman.typing import GSignals


class Manager(GObject.GObject, metaclass=SingletonGObjectMeta):
    __gsignals__: GSignals = {
        'session-added': (GObject.SignalFlags.NO_HOOKS, None, (str,)),
        'session-removed': (GObject.SignalFlags.NO_HOOKS, None, (str,)),
        'transfer-started': (GObject.SignalFlags.NO_HOOKS, None, (str,)),
        'transfer-completed': (GObject.SignalFlags.NO_HOOKS, None, (str, bool)),
    }

    connect_signal = GObject.GObject.connect
    disconnect_signal = GObject.GObject.disconnect

    __bus_name = 'org.bluez.obex'

    def __init__(self) -> None:
        super().__init__()
        self.__transfers: Dict[str, Transfer] = {}

        self._object_manager = Gio.DBusObjectManagerClient.new_for_bus_sync(
            Gio.BusType.SESSION, Gio.DBusObjectManagerClientFlags.NONE,
            self.__bus_name, '/', None, None, None)

        self._object_manager.connect('object-added', self._on_object_added)
        self._object_manager.connect('object-removed', self._on_object_removed)

        weakref.finalize(self, self._on_delete)

    def _on_delete(self) -> None:
        self._object_manager.disconnect_by_func(self._on_object_added)
        self._object_manager.disconnect_by_func(self._on_object_removed)

    def _on_object_added(self, _object_manager: Gio.DBusObjectManager, dbus_object: Gio.DBusObject) -> None:
        session_proxy = dbus_object.get_interface('org.bluez.obex.Session1')
        transfer_proxy = dbus_object.get_interface('org.bluez.obex.Transfer1')
        object_path = dbus_object.get_object_path()

        if transfer_proxy:
            logging.info(object_path)
            transfer = Transfer(object_path)
            transfer.connect_signal('completed', self._on_transfer_completed, True)
            transfer.connect_signal('error', self._on_transfer_completed, False)
            self.__transfers[object_path] = transfer
            self.emit('transfer-started', object_path)

        if session_proxy:
            logging.info(object_path)
            self.emit('session-added', object_path)

    def _on_object_removed(self, _object_manager: Gio.DBusObjectManager, dbus_object: Gio.DBusObject) -> None:
        session_proxy = dbus_object.get_interface('org.bluez.obex.Session1')
        transfer_proxy = dbus_object.get_interface('org.bluez.obex.Transfer1')
        object_path = dbus_object.get_object_path()

        if transfer_proxy and object_path in self.__transfers:
            logging.info(object_path)
            transfer = self.__transfers.pop(object_path)

            transfer.disconnect_by_func(self._on_transfer_completed, True)
            transfer.disconnect_by_func(self._on_transfer_completed, False)

        if session_proxy:
            logging.info(object_path)
            self.emit('session-removed', object_path)

    def _on_transfer_completed(self, transfer: Transfer, success: bool) -> None:
        transfer_path = transfer.get_object_path()

        logging.info("%s %s" % (transfer_path, success))
        self.emit('transfer-completed', transfer_path, success)

    @classmethod
    def watch_name_owner(
        cls,
        appeared_handler: Callable[[Gio.DBusConnection, str, str], None],
        vanished_handler: Callable[[Gio.DBusConnection, str], None],
    ) -> None:
        Gio.bus_watch_name(Gio.BusType.SESSION, cls.__bus_name, Gio.BusNameWatcherFlags.AUTO_START,
                           appeared_handler, vanished_handler)
>>>>>>> upstream/master
