<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.Functions import dprint
from blueman.bluez.obex.Base import Base
from gi.repository import GObject


class Client(Base):
    __gsignals__ = {
        str('session-created'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT,)),
        str('session-failed'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT,)),
        str('session-removed'): (GObject.SignalFlags.NO_HOOKS, None, ()),
    }

    def __init__(self):
        if self.__class__.get_interface_version()[0] < 5:
            super(Client, self).__init__('org.bluez.obex.Client', '/', True)
        else:
            super(Client, self).__init__('org.bluez.obex.Client1', '/org/bluez/obex')

    def create_session(self, dest_addr, source_addr="00:00:00:00:00:00", pattern="opp"):
        def on_session_created(session_path):
            dprint(dest_addr, source_addr, pattern, session_path)
            self.emit("session-created", session_path)

        def on_session_failed(error):
            dprint(dest_addr, source_addr, pattern, error)
            self.emit("session-failed", error)

        self._interface.CreateSession(dest_addr, {"Source": source_addr, "Target": pattern},
                                      reply_handler=on_session_created, error_handler=on_session_failed)

    def remove_session(self, session_path):
        def on_session_removed():
            dprint(session_path)
            self.emit('session-removed')

        def on_session_remove_failed(error):
            dprint(session_path, error)

        self._interface.RemoveSession(session_path, reply_handler=on_session_removed,
                                      error_handler=on_session_remove_failed)
=======
# coding=utf-8
import logging

from blueman.bluez.errors import BluezDBusException
from blueman.bluez.obex.Base import Base
from gi.repository import GObject, GLib

from blueman.typing import GSignals


class Client(Base):
    __gsignals__: GSignals = {
        'session-failed': (GObject.SignalFlags.NO_HOOKS, None, (str,)),
    }

    _interface_name = 'org.bluez.obex.Client1'

    def __init__(self) -> None:
        super().__init__(interface_name=self._interface_name, obj_path='/org/bluez/obex')

    def create_session(self, dest_addr: str, source_addr: str = "00:00:00:00:00:00", pattern: str = "opp") -> None:
        def on_session_created(session_path: str) -> None:
            logging.info("%s %s %s %s" % (dest_addr, source_addr, pattern, session_path))

        def on_session_failed(error: BluezDBusException) -> None:
            logging.error("%s %s %s %s" % (dest_addr, source_addr, pattern, error))
            self.emit("session-failed", error)

        v_source_addr = GLib.Variant('s', source_addr)
        v_pattern = GLib.Variant('s', pattern)
        param = GLib.Variant('(sa{sv})', (dest_addr, {"Source": v_source_addr, "Target": v_pattern}))
        self._call('CreateSession', param, reply_handler=on_session_created, error_handler=on_session_failed)

    def remove_session(self, session_path: str) -> None:
        def on_session_removed() -> None:
            logging.info(session_path)

        def on_session_remove_failed(error: BluezDBusException) -> None:
            logging.error("%s %s" % (session_path, error))

        param = GLib.Variant('(o)', (session_path,))
        self._call('RemoveSession', param, reply_handler=on_session_removed,
                   error_handler=on_session_remove_failed)
>>>>>>> upstream/master
