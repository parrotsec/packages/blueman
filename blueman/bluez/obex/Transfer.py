<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.Functions import dprint
from blueman.bluez.obex.Base import Base
from gi.repository import GObject


class Transfer(Base):
    __gsignals__ = {
        str('progress'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT,)),
        str('completed'): (GObject.SignalFlags.NO_HOOKS, None, ()),
        str('error'): (GObject.SignalFlags.NO_HOOKS, None, (GObject.TYPE_PYOBJECT,))
    }

    def __init__(self, transfer_path):
        if self.__class__.get_interface_version()[0] < 5:
            super(Transfer, self).__init__('org.bluez.obex.Transfer', transfer_path, True)

            handlers = {
                'PropertyChanged': self._on_property_changed,
                'Complete': self._on_complete,
                'Error': self._on_error
            }

            for signal, handler in handlers.items():
                self._handle_signal(handler, signal)
        else:
            super(Transfer, self).__init__('org.freedesktop.DBus.Properties', transfer_path)
            self._handle_signal(self._on_properties_changed, 'PropertiesChanged')

    def __getattr__(self, name):
        if name in ('filename', 'name', 'session', 'size'):
            if self.__class__.get_interface_version()[0] < 5:
                raise NotImplementedError()
            else:
                return self._interface.Get('org.bluez.obex.Transfer1', name.capitalize())

    def _on_property_changed(self, name, value):
        if name == 'Progress':
            dprint(self.object_path, name, value)
            self.emit('progress', value)

    def _on_complete(self):
        dprint(self.object_path)
        self.emit('completed')

    def _on_error(self, code, message):
        dprint(self.object_path, code, message)
        self.emit('error', message)

    def _on_properties_changed(self, interface_name, changed_properties, _invalidated_properties):
        if interface_name != 'org.bluez.obex.Transfer1':
            return

        for name, value in changed_properties.items():
            dprint(self.object_path, name, value)
=======
# coding=utf-8
import logging
from typing import List, Optional

from blueman.bluez.obex.Base import Base
from gi.repository import GObject, GLib

from blueman.typing import GSignals


class Transfer(Base):
    __gsignals__: GSignals = {
        'progress': (GObject.SignalFlags.NO_HOOKS, None, (int,)),
        'completed': (GObject.SignalFlags.NO_HOOKS, None, ()),
        'error': (GObject.SignalFlags.NO_HOOKS, None, ())
    }

    _interface_name = 'org.bluez.obex.Transfer1'

    def __init__(self, transfer_path: str):
        super().__init__(interface_name=self._interface_name, obj_path=transfer_path)

    @property
    def filename(self) -> Optional[str]:
        name: Optional[str] = self.get("Filename")
        return name

    @property
    def name(self) -> str:
        name: str = self.get("Name")
        return name

    @property
    def session(self) -> str:
        session: str = self.get("Session")
        return session

    @property
    def size(self) -> Optional[int]:
        size: Optional[int] = self.get("Size")
        return size

    def do_g_properties_changed(self, changed_properties: GLib.Variant, _invalidated_properties: List[str]) -> None:
        for name, value in changed_properties.unpack().items():
            logging.debug("%s %s %s" % (self.get_object_path(), name, value))
>>>>>>> upstream/master
            if name == 'Transferred':
                self.emit('progress', value)
            elif name == 'Status':
                if value == 'complete':
                    self.emit('completed')
                elif value == 'error':
<<<<<<< HEAD
                    self.emit('error', None)
=======
                    self.emit('error')
>>>>>>> upstream/master
