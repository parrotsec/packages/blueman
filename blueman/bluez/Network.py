<<<<<<< HEAD
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
from __future__ import unicode_literals

from blueman.bluez.PropertiesBlueZInterface import PropertiesBlueZInterface
from blueman.bluez.errors import raise_dbus_error


class Network(PropertiesBlueZInterface):
    @raise_dbus_error
    def __init__(self, obj_path=None):
        if self.__class__.get_interface_version()[0] < 5:
            interface = 'org.bluez.Network'
        else:
            interface = 'org.bluez.Network1'

        super(Network, self).__init__(interface, obj_path)

    @raise_dbus_error
    def connect(self, uuid, reply_handler=None, error_handler=None):
        self.get_interface().Connect(uuid, reply_handler=reply_handler, error_handler=error_handler)

    @raise_dbus_error
    def disconnect(self, reply_handler=None, error_handler=None):
        self.get_interface().Disconnect(reply_handler=reply_handler, error_handler=error_handler)
=======
# coding=utf-8
from typing import Optional, Callable

from blueman.bluez.Base import Base
from blueman.bluez.AnyBase import AnyBase
from gi.repository import GLib

from blueman.bluez.errors import BluezDBusException


class Network(Base):
    _interface_name = 'org.bluez.Network1'

    def __init__(self, obj_path: str):
        super().__init__(interface_name=self._interface_name, obj_path=obj_path)

    def connect(
        self,
        uuid: str,
        reply_handler: Optional[Callable[[str], None]] = None,
        error_handler: Optional[Callable[[BluezDBusException], None]] = None,
    ) -> None:
        param = GLib.Variant('(s)', (uuid,))
        self._call('Connect', param, reply_handler=reply_handler, error_handler=error_handler)

    def disconnect(
        self,
        reply_handler: Optional[Callable[[], None]] = None,
        error_handler: Optional[Callable[[BluezDBusException], None]] = None,
    ) -> None:
        self._call('Disconnect', reply_handler=reply_handler, error_handler=error_handler)


class AnyNetwork(AnyBase):
    def __init__(self) -> None:
        super().__init__('org.bluez.Network1')
>>>>>>> upstream/master
